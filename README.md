# Chess game project - OOP practice

## Develop with tracking memory leaks

- Code 
```cpp
// main.cpp
#define __GPP_COMPILER__ // memory leaks check when testing
```

- Compile
```bash
g++ -g main.cpp \
Chess/src/chess_position.cpp \
Chess/src/chessgameengine.cpp \
Pieces/src/piece.cpp \
Pieces/src/king.cpp \
Pieces/src/queen.cpp \
Pieces/src/rook.cpp \
Pieces/src/bishop.cpp \
Pieces/src/knight.cpp \
Pieces/src/pawn.cpp \
Tests/chessgameenginetest.cpp \
Tests/piecesmanualtest.cpp \
Tests/chesspositiontest.cpp
```

- Use
```bash
valgrind --leak-check=full ./a.out
```
- Install
```bash
sudo apt install valgrind
```


## Class Diagram Extended
```plantuml
' Split into 2 pages
page 2x1

/' Chess game  '/
Class ChessGameEngine {
 board: Piece[8][8]
 +startGame()
 +move(Piece, Position)
 +isLegalMove(Piece, Position)
 +nextToPlay()
 #isCheck()
 #calcCheckedMoves()
 #isMate()
 #showMoves(Piece)
 #calcLegalMoves()
 #getNextMove()
 #canCastle()
 #Castle()
 #canEnpessant()
 #enpessant()
}

/'
Class ChessEngine {
  getNextMove();
  setMove(?);
}
'/

Class GUI {
 +updateBoard()
 +displayMoves(Piece)
 +remake()
}

/' Chess position '/

Class ChessPosition {
  -x: int8_t
  -y: int8_t

  -set(int8_t x, int8_t y): void
  -clear():void
}

/' Pieces '/
Class Piece {
  #value: byte
  #movepattern: byte
  #color: bool/byte
  #type: enum_piece
  #position: ChessPosition
  move(): void
  -getMoves(ChessPosition* moves, uint8_t* count) : uint8_t
}

Class Queen {
  -getMoves($) : uint8_t
}

Class King {
  -castlePrivilege: bool
  -move(): void
  -canCastle(): bool
  -getMoves($) : uint8_t
}

Class Rook {
  -castlePrivilege: bool
  -move(): void
  -canCastle(): bool
  -getMoves($) : uint8_t
}
Class Bishop {
  -getMoves($) : uint8_t
}
Class Knight {
  -getMoves($) : uint8_t
}
Class Pawn {
  #isMoved: bool;
  -promote(): void
  -move(): void
  -getMoves($) : uint8_t
}

/' Piece relations '/
Queen -up.-|> Piece
King -up.-|> Piece
Rook -up.-|> Piece
Bishop -up.-|> Piece
Knight -up.-|> Piece
Pawn -up.-|> Piece


/' -------------------- Classes relations -------------------- '/
/'ChessEngine "1" -right- "1" ChessGameEngine : "            "'/ /' distance between '/
/'Player "2?" -- "1"  ChessGameEngine'/
Piece -up- ChessGameEngine
GUI "0..*"  -left.-|> "1"  ChessGameEngine : "            " /' distance between '/


/' ChessGameEngine "1" -- "1..*" Piece '/
ChessPosition -left- Piece
ChessPosition -up- GUI
ChessPosition -up- ChessGameEngine


/' -------------------- Classes Testing -------------------- '/
Class PiecesManualTest {
#board: char[8][8]

#moves: Position*
#movesCount: uint8_t

-resetBoard(): void
-updateBoard(): void
-printBoard(): void

-printRank(uint8_t rank): void
-printFile(uint8_t file): void

-printBoardsRank(uint8_t rank): void
-printBoardsFile(uint8_t rank): void

-testPiece(enum_chess_piece piece): void

-getPieceMoves(enum_chess_piece piece, ChessPosition pos): void <- TODO

-run(): void
}

Class ChessGameEngineTest {
-printBoard(): void
-run(): void
}

ChessGameEngineTest -right.-|> ChessGameEngine : "            " /' distance between '/

PiecesManualTest -up.-|> Queen
PiecesManualTest -up.-|> King
PiecesManualTest -up.-|> Rook
PiecesManualTest -up.-|> Bishop
PiecesManualTest -up.-|> Knight
PiecesManualTest -up.-|> Pawn

enum enum_chess_piece {
 None
 King
 Queen
 Rook
 Bishop
 Knight
 Pawn
}

enum enum_chess_color {
 White
 Black
}

enum_chess_piece -up[hidden]- PiecesManualTest
enum_chess_color -up[hidden]- PiecesManualTest

```

