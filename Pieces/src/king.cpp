#include "../inc/king.h"
#include "../../Chess/inc/chess_position.h"

King::King() :
  moved(false)
{
  _init(true);
}

King::King(const uint8_t& color) :
  moved(false)
{
  _init(false);

  this->pos = ChessPosition();

  switch(color)
  {
  case CPL_BLACK:
  case CPL_WHITE:
    this->color = color;
  default:
    break;
  }

}

King::King(const ChessPosition& pos) :
  moved(false)
{
  _init(false);
  this->pos = pos;
}

King::King(const ChessPosition& pos, const uint8_t& color) :
  moved(false)
{
  _init(false);

  this->pos = pos;

  switch(color)
  {
  case CPL_BLACK:
  case CPL_WHITE:
    this->color = color;
  default:
    break;
  }
}

void King::_init(uint8_t isBare)
{
  if ( isBare )
  {
    this->pos = ChessPosition();
    this->color = CPL_NONE;
  }

  this->type = P_KING;
  this->value = VAL_KING;
  this->move_pattern = M_CASTLE | M_SQUERE;
}

uint8_t King::move(const ChessPosition& pos)
{
  if ( !isValidPosition(this->pos) )
  {
    return ERR_INV_POS;
  }

  moved = true;

  this->pos = pos;

  return MOVE_OK;
}

uint8_t King::hasMoved()
{
  return moved;
}

/**
 * @brief King::getMoves
 * @param moves
 * @param size
 * @return 0      - succes (MOVE_OK)
 *         other  - ERR_CODE
 *
 * Note: Free up memory of *moves before passing as arg.
 *       *moves will be lost and replaced with new ones.
 */
uint8_t King::getMoves(ChessPosition **moves, uint16_t *size)
{
  if ( !isValidPosition(this->pos) )
  {
    return ERR_INV_POS;
  }

  uint16_t valid_moves = 8;
  // Calculate valid positions

  if( pos.y == POS_MIN || pos.y ==  POS_MAX )
  {
    if (pos.x == POS_MIN || pos.x == POS_MAX)
    {
      valid_moves = 3; // - (1row + 1 col)
    }
    else
    {
      valid_moves = 5; // - 1 row
    }
  }

  // Allocate mem of positions
  if ( *moves != nullptr )
    delete[] *moves; // Clear previous memory allocations

  ChessPosition *genMoves = new ChessPosition[valid_moves];
  *moves = genMoves;

  // Write coordinate for every position
  // todo: Flag direction !
  // for pieces that can't jump over other pieces
  // group together moves that can be blocked ( to be skipped quicker )
  *size = 0;

  getMovesDist(this->pos, 1, M_DIAGONAL | M_HOR_VER, *moves,  size);

  return MOVE_OK;
}
