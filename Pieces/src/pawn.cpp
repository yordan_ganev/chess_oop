#include "../inc/pawn.h"
#include "../inc/piece.h"

Pawn::Pawn() :
  moved(false)
{
  _init(true);
}


Pawn::Pawn(const uint8_t& color) :
  moved(false)
{
  _init(false);

  this->pos = ChessPosition();

  switch(color)
  {
  case CPL_BLACK:
  case CPL_WHITE:
    this->color = color;
  default:
    break;
  }

}

Pawn::Pawn(const ChessPosition& pos) :
  moved(false)
{
  _init(false);
  this->pos = pos;
}

Pawn::Pawn(const ChessPosition& pos, const uint8_t& color) :
  moved(false)
{
  _init(false);

  this->pos = pos;

  switch(color)
  {
  case CPL_BLACK:
  case CPL_WHITE:
    this->color = color;
  default:
    break;
  }
}

void Pawn::_init(uint8_t isBare)
{
  if ( isBare )
  {
    this->pos = ChessPosition();
    this->color = CPL_NONE;
  }

  this->type = P_PAWN;
  this->value = VAL_PAWN;
  this->move_pattern = M_FORWARD | M_EN_PASSANT;
}

uint8_t Pawn::hasMoved()
{
  /*
  if ( color == CPL_WHITE )
  {
    return (pos.y == POS_MIN+1);
  }

  if ( color == CPL_BLACK )
  {
    return (pos.y == POS_MAX-1);
  }
  */

  return moved;
}

uint8_t Pawn::move(const ChessPosition& pos)
{
  if ( !isValidPosition(this->pos) )
  {
    return ERR_INV_POS;
  }

  moved = true;

  this->pos = pos;

  return MOVE_OK;
}

/**
 * @brief Pawn::getMoves
 * @param moves
 * @param size
 * @return 0      - succes (MOVE_OK)
 *         other  - ERR_CODE
 *
 * Note: Free up memory of *moves before passing as arg.
 *       *moves will be lost and replaced with new ones.
 */
uint8_t Pawn::getMoves(ChessPosition **moves, uint16_t *size)
{
  if ( !isValidPosition(this->pos) )
  {
    return ERR_INV_POS;
  }

  uint16_t valid_moves = 4;
  // Calculate valid positions

  valid_moves -= ( pos.x == POS_MIN || pos.x == POS_MAX );
  valid_moves -= ( hasMoved() );

  // Allocate mem of positions
  if ( *moves != nullptr )
    delete[] *moves; // Clear previous memory allocations

  ChessPosition *genMoves = new ChessPosition[valid_moves];
  *moves = genMoves;

  // Write coordinate for every position
  // todo: Flag direction !
  // for pieces that can't jump over other pieces
  // group together moves that can be blocked ( to be skipped quicker )
  *size = 0;

  int count = 0;
  if ( color == CPL_WHITE )
  {
    genMoves[count++].set(pos.x, pos.y + 1);

    if ( !hasMoved() )
    {
      genMoves[count++].set(pos.x, pos.y + 2);
    }

    if ( pos.x != POS_MIN )
    {
      genMoves[count++].set(pos.x - 1, pos.y + 1);
    }

    if ( pos.x != POS_MAX )
    {
      genMoves[count++].set(pos.x + 1, pos.y + 1);
    }
  }
  else
  {
      genMoves[count++].set(pos.x, pos.y - 1);
      if ( !hasMoved() )
      {
        genMoves[count++].set(pos.x, pos.y - 2);
      }

      if ( pos.x != POS_MIN )
      {
        genMoves[count++].set(pos.x - 1, pos.y - 1);
      }

      if ( pos.x != POS_MAX )
      {
        genMoves[count++].set(pos.x + 1, pos.y - 1);
      }
  }

  *size = count;

  return MOVE_OK;
}
