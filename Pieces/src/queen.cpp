#include "../inc/piece.h"
#include "../inc/queen.h"
#include "../../Chess/inc/chess_position.h"

#include <stdint.h>

Queen::Queen()
{
  _init(true);
}

Queen::Queen(const uint8_t& color)
{
  _init(false);

  this->pos = ChessPosition();

  switch(color)
  {
  case CPL_BLACK:
  case CPL_WHITE:
    this->color = color;
  default:
    break;
  }

}

Queen::Queen(const ChessPosition& pos)
{
  _init(false);
  this->pos = pos;
}

Queen::Queen(const ChessPosition& pos, const uint8_t& color)
{
  _init(false);

  this->pos = pos;

  switch(color)
  {
  case CPL_BLACK:
  case CPL_WHITE:
    this->color = color;
  default:
    break;
  }
}

void Queen::_init(uint8_t isBare)
{
  if ( isBare )
  {
    this->pos = ChessPosition();
    this->color = CPL_NONE;
  }

  this->type = P_QUEEN;
  this->value = VAL_QUEEN;
  this->move_pattern = (M_HOR_VER | M_DIAGONAL);
}

/**
 * @brief Queen::getMoves
 * @param moves
 * @param size
 * @return 0      - succes (MOVE_OK)
 *         other  - ERR_CODE
 *
 * Note: Free up memory of *moves before passing as arg.
 *       *moves will be lost and replaced with new ones.
 */
uint8_t Queen::getMoves(ChessPosition **moves, uint16_t *size)
{
  if ( !isValidPosition(this->pos) )
  {
    return ERR_INV_POS;
  }

  uint16_t valid_moves = 0;
  // Calculate valid positions
  valid_moves += 14; // horizontal moves always 7+7 (horizontal + vertical) possible tails

  // diagonal moves count 7 on edge to 13 in the center, changes by step 2
  valid_moves += ( pos.x < pos.y ) ? pos.x : pos.y;                           // top left
  valid_moves += ( pos.x < POS_MAX - pos.y ) ? pos.x : POS_MAX - pos.y;       //top right
  valid_moves += POS_MAX - (( pos.x > POS_MAX - pos.y ) ? pos.x : POS_MAX - pos.y); //bottom left
  valid_moves += POS_MAX - (( pos.x > pos.y ) ? pos.x : pos.y);               // bottom right

  // Allocate mem of positions
  if ( *moves != nullptr )
    delete[] *moves; // Clear previous memory allocations

  *moves = new ChessPosition[valid_moves];

  ChessPosition *base = *moves;

  *size = valid_moves;
  // group together moves that can be blocked ( to be skipped quicker )
  // Start by closest squeres and clockwise rotate to end of furtherest

  uint16_t size_n = 0;
  for (int i = 1; i <= POS_MAX; i++)
  {
    getMovesDist(pos, i, M_HOR_VER | M_DIAGONAL, base, &size_n);
    base += size_n;
    valid_moves -= size_n;
    if (!valid_moves)
    {
      break;
    }
  }

  return MOVE_OK;
}
