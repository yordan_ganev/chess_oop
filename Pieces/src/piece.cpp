#include "../inc/piece.h"
#include "stdint.h"

Piece::Piece() :
  pos(ChessPosition()),
  type(P_NONE),
  value(0),
  color(CPL_NONE),
  move_pattern(0)
{
}

Piece::Piece(E_PIECE_TYPE type) :
  pos(ChessPosition()),
  type(type)
{
  __init_piece();
}

Piece::~Piece()
{
}


Piece::Piece(E_PIECE_TYPE type, ChessPosition pos) :
  pos(pos),
  type(type)
{
  __init_piece();
}

void Piece::__init_piece()
{
  switch(type)
  {
  case P_KING:
    value = 0;
    move_pattern = (M_SQUERE);
    break;
  case P_QUEEN:
    value = VAL_QUEEN;
    move_pattern = (M_HOR_VER | M_DIAGONAL);
    break;
  case P_BISHOP:
    value = VAL_BISHOP;
    move_pattern = (M_DIAGONAL);
    break;
  case P_KNIGHT:
    value = VAL_KNIGHT;
    move_pattern = (M_L_JUMP);
    break;
  case P_ROOK:
    value = VAL_ROOK;
    move_pattern = (M_HOR_VER);
    break;
  default:
    value = 0;
    move_pattern = 0;
    break;
  }
}

uint8_t Piece::getMovePattern() const
{
  return move_pattern;
}

void Piece::setMovePattern(uint8_t newMove_pattern)
{
  move_pattern = newMove_pattern;
}
