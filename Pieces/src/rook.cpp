#include "../inc/rook.h"
#include "../../Chess/inc/chess_position.h"

Rook::Rook() :
  moved(false)
{
  _init(true);
}


Rook::Rook(const uint8_t& color) :
  moved(false)
{
  _init(false);

  this->pos = ChessPosition();

  switch(color)
  {
  case CPL_BLACK:
  case CPL_WHITE:
    this->color = color;
  default:
    break;
  }

}

Rook::Rook(const ChessPosition& pos) :
  moved(false)
{
  _init(false);
  this->pos = pos;
}

Rook::Rook(const ChessPosition& pos, const uint8_t& color) :
  moved(false)
{
  _init(false);

  this->pos = pos;

  switch(color)
  {
  case CPL_BLACK:
  case CPL_WHITE:
    this->color = color;
  default:
    break;
  }
}

void Rook::_init(uint8_t isBare)
{
  if ( isBare )
  {
    this->pos = ChessPosition();
    this->color = CPL_NONE;
  }

  this->type = P_ROOK;
  this->value = VAL_ROOK;
  this->move_pattern = M_HOR_VER;
}

uint8_t Rook::move(const ChessPosition& pos)
{
  if ( !isValidPosition(this->pos) )
  {
    return ERR_INV_POS;
  }

  moved = true;

  this->pos = pos;

  return MOVE_OK;
}


uint8_t Rook::hasMoved()
{
  return moved;
}


/**
 * @brief Rook::getMoves
 * @param moves
 * @param size
 * @return 0      - succes (MOVE_OK)
 *         other  - ERR_CODE
 *
 * Note: Free up memory of *moves before passing as arg.
 *       *moves will be lost and replaced with new ones.
 */
uint8_t Rook::getMoves(ChessPosition **moves, uint16_t *size)
{
  if ( !isValidPosition(this->pos) )
  {
    return ERR_INV_POS;
  }

  uint16_t valid_moves = 14;
  // Calculate valid positions


  // Allocate mem of positions
  if ( *moves != nullptr )
    delete[] *moves; // Clear previous memory allocations

  *moves = new ChessPosition[valid_moves];

  ChessPosition *base = *moves;

  *size = valid_moves;

  // Write coordinate for every position
  // todo: Flag direction !
  // for pieces that can't jump over other pieces
  // group together moves that can be blocked ( to be skipped quicker )
  uint16_t size_n = 0;
  for (int i = 1; i <= POS_MAX; i++)
  {
    getMovesDist(pos, i, M_HOR_VER, base, &size_n);
    base += size_n;
    valid_moves -= size_n;
    if (!valid_moves)
    {
      break;
    }
  }

  return MOVE_OK;
}
