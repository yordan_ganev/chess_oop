#include "../inc/knight.h"
#include "../../Chess/inc/chess_position.h"

Knight::Knight() :
  moved(false)
{
  _init(true);
}


Knight::Knight(const uint8_t& color) :
  moved(false)
{
  _init(false);

  this->pos = ChessPosition();

  switch(color)
  {
  case CPL_BLACK:
  case CPL_WHITE:
    this->color = color;
  default:
    break;
  }

}

Knight::Knight(const ChessPosition& pos) :
  moved(false)
{
  _init(false);
  this->pos = pos;
}

Knight::Knight(const ChessPosition& pos, const uint8_t& color) :
  moved(false)
{
  _init(false);

  this->pos = pos;

  switch(color)
  {
  case CPL_BLACK:
  case CPL_WHITE:
    this->color = color;
  default:
    break;
  }
}

void Knight::_init(uint8_t isBare)
{
  if ( isBare )
  {
    this->pos = ChessPosition();
    this->color = CPL_NONE;
  }

  this->type = P_KNIGHT;
  this->value = VAL_KNIGHT;
  this->move_pattern = M_L_JUMP;
}

//uint8_t Knight::move(const ChessPosition& pos)
//{
//  if ( !isValidPosition(this->pos) )
//  {
//    return ERR_INV_POS;
//  }
//  return MOVE_OK;
//}

/**
 * @brief Knight::getMoves
 * @param moves
 * @param size
 * @return 0      - succes (MOVE_OK)
 *         other  - ERR_CODE
 *
 * Note: Free up memory of *moves before passing as arg.
 *       *moves will be lost and replaced with new ones.
 */
uint8_t Knight::getMoves(ChessPosition **moves, uint16_t *size)
{
  if ( !isValidPosition(this->pos) )
  {
    return ERR_INV_POS;
  }

  uint16_t valid_moves = 0;
  const uint8_t KNIGHT_MAX_MOVES = 8;
  // Calculate valid positions

  /*
   * Check for rectangles with diagonals
   * Quick pass threw diagonal quadrants
   * Get actuall moves count without checking each possible
   * Round remaining moves to 4 to skip unnecessary comparisons
   *
   * Checks cover 81.25% (52/64) of possible moves
   *
   * |2|3|4|4|4|4|3|2|
   * |3|4|6|6|6|6|4|3|
   * |4|6|8|8|8|8|6|4|
   * |4|6|8|8|8|8|6|4|
   * |4|6|8|8|8|8|6|4|
   * |4|6|8|8|8|8|6|4|
   * |3|4|6|6|6|6|4|3|
   * |2|3|4|4|4|4|3|2|
   *
   * Good for now since one or two more allocations should not be
   * a huge problem because it's only for knight moves.
   */


  bool quadrant_2 = isValidPosition(pos.x + 2, pos.y + 2);
  bool quadrant_4 = isValidPosition(pos.x - 2, pos.y - 2);

  if ( quadrant_2 )
  {
    if ( quadrant_4 )
    {
      valid_moves = KNIGHT_MAX_MOVES;
    }
    else if ( isValidPosition(pos.x-1, pos.y -2) )
    {
      valid_moves = 6;
    }
    else if ( isValidPosition(pos.x-2, pos.y -1) )
    {
      valid_moves = 6;
    }
    else
    {
      valid_moves = 4;
    }
  }
  else if ( quadrant_4 )
  {
    if ( isValidPosition(pos.x + 1, pos.y + 2) )
    {
      valid_moves = 6;
    }
    else if ( isValidPosition(pos.x + 2, pos.y + 1) )
    {
      valid_moves = 6;
    }
    else
    {
      valid_moves = 4;
    }
  }
  else
  {
    valid_moves = 4;
  }


  // Allocate mem of positions
  if ( *moves != nullptr )
    delete[] *moves; // Clear previous memory allocations

  *moves = new ChessPosition[valid_moves];

  // Write coordinate for every position
  // Coordinates of moves clockwise dir stars 12o'clock

  struct _position {
    int8_t x;
    int8_t y;
  };

  _position delta[KNIGHT_MAX_MOVES] = {
    {1, 2},
    {2, 1},
    {2, -1},
    {1, -2},
    {-1, -2},
    {-2, -1},
    {-2, 1},
    {-1, 2}
  };

  uint16_t size_n = 0;


  if ( valid_moves == KNIGHT_MAX_MOVES )
  {
    for ( int i = 0; i < KNIGHT_MAX_MOVES; i++)
    {
      (*moves)[i].set(pos.x + delta[i].x, pos.y + delta[i].y);
    }
    size_n = KNIGHT_MAX_MOVES;
  }
  else
  {
    for ( int i = 0; i < KNIGHT_MAX_MOVES; i++)
    {
      int8_t x = pos.x + delta[i].x;
      int8_t y = pos.y + delta[i].y;

      if ( isValidPosition(x, y) )
      {
        (*moves)[size_n++].set(x, y);
      }
    }

  }

  *size = size_n;

  return MOVE_OK;
}
