#ifndef __BISHOP_H__
#define __BISHOP_H__

#include "piece.h"

class Bishop : public Piece
{
public:
  Bishop();
  Bishop(const uint8_t& color);
  Bishop(const ChessPosition& pos);
  Bishop(const ChessPosition& pos, const uint8_t& color);

  uint8_t getMoves(ChessPosition **moves, uint16_t *size) override;

private:
  void _init(uint8_t isBare);
};

#endif // __BISHOP_H__
