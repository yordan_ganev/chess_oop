#ifndef ROOK_H
#define ROOK_H

#include "piece.h"

class Rook : public Piece
{
protected:
  uint8_t moved;

public:
  Rook();
  Rook(const uint8_t& color);
  Rook(const ChessPosition& pos);
  Rook(const ChessPosition& pos, const uint8_t& color);

  uint8_t move(const ChessPosition& pos) override;
  uint8_t getMoves(ChessPosition **moves, uint16_t *size) override;

  uint8_t hasMoved(); // isMoved

private:
  void _init(uint8_t isBare);
};

#endif // ROOK_H
