#ifndef KNIGHT_H
#define KNIGHT_H

#include "piece.h"

class Knight : public Piece
{
protected:
  uint8_t moved;

public:
  Knight();
  Knight(const uint8_t& color);
  Knight(const ChessPosition& pos);
  Knight(const ChessPosition& pos, const uint8_t& color);

//  uint8_t move(const ChessPosition& pos) override;
  uint8_t getMoves(ChessPosition **moves, uint16_t *size) override;

  uint8_t hasMoved(); // isMoved

private:
  void _init(uint8_t isBare);
};

#endif // KNIGHT_H
