#ifndef QUEEN_H
#define QUEEN_H

#include "piece.h"

class Queen : public Piece
{
public:
  Queen();
  Queen(const uint8_t& color);
  Queen(const ChessPosition& pos);
  Queen(const ChessPosition& pos, const uint8_t& color);

  uint8_t getMoves(ChessPosition **moves, uint16_t *size) override;

private:
  void _init(uint8_t isBare);
};

#endif // QUEEN_H
