#ifndef PIECE_H
#define PIECE_H

#include <stdint.h>

#include "../../Chess/inc/chess_position.h"

// Figure types
enum E_PIECE_TYPE {
  P_KING,
  P_QUEEN,
  P_ROOK,
  P_BISHOP,
  P_KNIGHT,
  P_PAWN,
  P_NONE
};

// Chess Players
enum {
  CPL_WHITE,
  CPL_BLACK,
  CPL_NONE
};

// Chess piece values
#define VAL_KING    111 // needed (?)
#define VAL_PAWN    1
#define VAL_KNIGHT  3
#define VAL_BISHOP  3
#define VAL_ROOK    5
#define VAL_QUEEN   9


// Chess move notation definitions
#define CN_PAWN     //void
#define CN_KING     'K'
#define CN_QUEEN    'Q'
#define CN_BISHOP   'B'
#define CN_KNIGHT   'N'
#define CN_ROOK     'R'
#define CN_CAPUTRE  'x'
#define CN_PROMOTE  '='
#define CN_CHECK    '+'
#define CN_MATE     '#'
#define CN_CASTLE_QSIDE "0-0-0"
#define CN_CASTLE_KSIDE "0-0"

class Piece
{
private:
  void __init_piece();

protected:
  ChessPosition pos;
  uint8_t type;
  uint8_t value;
  uint8_t color;

  uint8_t move_pattern;
public:
  Piece();
  Piece(E_PIECE_TYPE type);
  Piece(E_PIECE_TYPE type, ChessPosition pos);

  virtual ~Piece();

  // overwriten by pawn/king/rook
  virtual uint8_t move(const ChessPosition& pos)
  {
    // common for every piece just change position values
    this->pos = pos;
    return MOVE_OK;
  };

  // for checking legal moves
  uint8_t setPosition(const ChessPosition& pos)
  {
    this->pos = pos;
    return MOVE_OK;
  };

  // todo ?
  virtual uint8_t getMoves(ChessPosition **moves, uint16_t *size)
  {
    return MOVE_ERR_INV_PATTERN;
  };

  uint8_t getType() {
    return type; // todo .cpp
  }

  uint8_t getColor() {
    return color; // todo .cpp
  }

  ChessPosition position() {return pos;};
  uint8_t getMovePattern() const;
  void setMovePattern(uint8_t newMove_pattern);
};
#endif // PIECE_H
