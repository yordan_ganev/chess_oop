#ifndef PAWN_H
#define PAWN_H

#include "piece.h"

class Pawn : public Piece
{
protected:
  uint8_t moved;

public:
  Pawn();
  Pawn(const uint8_t& color);
  Pawn(const ChessPosition& pos);
  Pawn(const ChessPosition& pos, const uint8_t& color);

  uint8_t move(const ChessPosition& pos) override;
  uint8_t getMoves(ChessPosition **moves, uint16_t *size) override;
  uint8_t hasMoved();

private:
  void _init(uint8_t isBare);
};


#endif // PAWN_H
