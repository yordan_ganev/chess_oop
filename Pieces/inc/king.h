#ifndef KING_H
#define KING_H

#include "piece.h"

class King : public Piece
{
protected:
  uint8_t moved;

public:
  King();
  King(const uint8_t& color);
  King(const ChessPosition& pos);
  King(const ChessPosition& pos, const uint8_t& color);

  uint8_t move(const ChessPosition& pos) override;
  uint8_t getMoves(ChessPosition **moves, uint16_t *size) override;

  uint8_t hasMoved(); // isMoved

private:
  void _init(uint8_t isBare);
};

#endif // KING_H
