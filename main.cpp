//#define __GPP_COMPILER__ // memory leaks check when testing

#ifndef __GPP_COMPILER__
#include "GUI/chessgui.h"
#include <QApplication>
#else
#include "Tests/piecesmanualtest.h"
#include "Tests/chessgameenginetest.h"
#include "Tests/chesspositiontest.h"
#endif

int main(int argc, char *argv[])
{

#ifndef __GPP_COMPILER__
  QApplication a(argc, argv);
  ChessGUI w;
  w.show();
  return a.exec();
#else
  ChessPositionTest().run();
  PiecesManualTest().run();
  ChessGameEngineTest().run();
#endif
}

