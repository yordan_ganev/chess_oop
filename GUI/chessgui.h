#ifndef CHESSGUI_H
#define CHESSGUI_H
//QT Widget
#include <QMainWindow>

#include <qwidget.h>
#include <qpainter.h>
#include <QResizeEvent>

// Chess
#include "../Chess/inc/chessgameengine.h"

QT_BEGIN_NAMESPACE
namespace Ui { class ChessGUI; }
QT_END_NAMESPACE

class ChessGUI : public QMainWindow, public ChessGameEngine
{
  Q_OBJECT

public:
  ChessGUI(QWidget *parent = nullptr);
  ~ChessGUI();

private slots:
  void on_actionThreat_Map_White_toggled(bool checked);

  void on_actionPiece_Legal_Moves_toggled(bool checked);

  void on_actionPiece_moves_toggled(bool checked);

  void on_actionThreat_Map_Black_toggled(bool checked);

  void on_actionNew_game_triggered();

  void on_actionEn_Passant_triggered();

  void on_actionEn_passant_triggered();

  void on_actionCheck_triggered();

  void on_actionCheck_mate_triggered();

  void on_actionEn_passant_pin_triggered();

  void on_actionPinned_piece_moves_triggered();

  void on_actionCastle_block_triggered();

  void on_actionCastle_triggered();

  void on_actionCheck_Blocking_triggered();

private:
  virtual void paintEvent(QPaintEvent *e);
  virtual void resizeEvent(QResizeEvent *e);
  virtual void mousePressEvent(QMouseEvent *e);
  Ui::ChessGUI *ui;

  float offset;

  float offsetX;
  float offsetY;

  int16_t sqrSize;

  void drawBoard(QPainter *painter);
  void drawPieces(QPainter *painter);
  void drawMoves(QPainter *painter);
  void drawThreats(QPainter *painter);
};
#endif // CHESSGUI_H
