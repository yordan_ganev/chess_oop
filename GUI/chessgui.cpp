// GUI Includes
#include "chessgui.h"
#include "ui_chessgui.h"
#include <math.h>
#include <qdir.h>

#include <Qt>
#include <QMessageBox>
//#include <algorithm>

//#include <qbrush.h>
//#include <qlabel.h>
//#include <qaction.h>

#include <vector>

// Chess Libraries
#include "../Chess/inc/chess_position.h"
#include "../Chess/inc/chessgameengine.h"
#include "../Pieces/inc/piece.h"
#include "../Pieces/inc/piece.h"
#include "../Pieces/inc/king.h"
#include "../Pieces/inc/queen.h"
#include "../Pieces/inc/rook.h"
#include "../Pieces/inc/bishop.h"
#include "../Pieces/inc/knight.h"
#include "../Pieces/inc/pawn.h"

#include <iostream>

static bool gameOver = false;

static bool drawThreatMapBlack = false;

static bool drawThreatMapWhite = false;

static bool drawPieceValidMoves = false;

static bool drawLegalMoves = true;

static bool signalChecks = true;


ChessGUI::ChessGUI(QWidget *parent) :
  QMainWindow(parent),
  ChessGameEngine(),
  ui(new Ui::ChessGUI),
  offset(0),
  offsetX(0),
  offsetY(0)
{
  ui->setupUi(this);
  startGame();
}

ChessGUI::~ChessGUI()
{
  delete ui;
}

using namespace std;

// ---- ---- ---- ---- Interface handlers ---- ---- ---- ----
void ChessGUI::paintEvent(QPaintEvent *e)
{
  QPainter painter(this);

  // paint bg color to black
  painter.setBrush(Qt::black);
  painter.drawRect(QRect(0, 0,  this->width(), this->height()));

  drawBoard(&painter);

  drawThreats(&painter);

  drawPieces(&painter);

  drawMoves(&painter);
}

void ChessGUI::mousePressEvent(QMouseEvent *e)
{

  if ( gameOver )
  {
    return;
  }

  if ( e->x() > offsetX && e->x() < this->width() - offsetX &&
       e->y() > offsetY && e->y() < this->height() - offsetY)
  {
    int boordCoordX = e->x() - offsetX;
    int boardCoordY = e->y() - offsetY;
    int file = boordCoordX / sqrSize;
    int rank = 7 - boardCoordY / sqrSize;

    // todo filter if piece is available for player
    vector<Piece *> pieces;
    getPieces(&pieces, nextToPlay());

    bool selected_piece = false;

    for ( uint i = 0; i < pieces.size(); i++)
    {
      if ( pieces.at(i)->position().x == file && pieces.at(i)->position().y == rank )
      {
        if ( selectedPosition().x != file || selectedPosition().y != rank )
        {
          // Castle check - king selected with rook position clicked
          if ( pieces.at(i)->getType() != P_ROOK || selectedPosition().x != getKingPosition().x ||  selectedPosition().y != getKingPosition().y )
          {
            select(file, rank);

            selected_piece = true;
            //printf("Select: (%d:%d)\r\n", pieces.at(i)->position().x, pieces.at(i)->position().y);
          }
        }
        else
        {
          //printf("%d:%d %d:%d\n\r", selectX, selectY, file, rank);

          //printf("Release: (%d:%d)\r\n", pieces.at(i)->position().x, pieces.at(i)->position().y);
          discard_select();
        }
        repaint();
        break;
      }
    }

    if ( !selected_piece )
    {
      for ( int i = 0; i < legal_moves_count; i++)
      {
        if ( legal_moves[i].x == file && legal_moves[i].y == rank )
        {
          ChessPosition pos = selectedPosition();
          this->ChessGameEngine::move(ChessPosition(file, rank));
          //printf("(%d:%d)=>(%d:%d)\r\n", pos.x, pos.y, file, rank);

          discard_select();
          repaint();
          if ( isCheckMate() )
          {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Checkmate!");
            if (nextToPlay() == CPL_WHITE)
            {
              msgBox.setText("Black won the game!");
            }
            else if (nextToPlay() == CPL_BLACK)
            {

              msgBox.setText("Checkmate! Black won the game!");
            }
            msgBox.exec();

            gameOver = true;
          }
          else if ( isDraw() )
          {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Draw!");
            msgBox.setText("Game resulted in draw.");
            msgBox.exec();

            gameOver = true;
          }
          break;
        }
      }

    }
  }
}

void ChessGUI::resizeEvent(QResizeEvent *e)
{
  repaint();
}

// ---- ---- ---- ---- Pixel Painting ---- ---- ---- ----

void ChessGUI::drawBoard(QPainter *painter)
{
  float boardX = 0;
  float boardY = 0;

  // Center the board
  painter->viewport().width();

  menuBar()->geometry().height();
  int tmp_height = painter->window().height();
  int tmp_width = painter->window().width();

  const int menuBarOffset = menuBar()->geometry().height();
 // const int menuBarOffset = this->frameSize().height() - painter->window().height();

  offset = (fmax(tmp_width, tmp_height-menuBarOffset) - fmin(tmp_width, tmp_height-menuBarOffset)) / 2;

  sqrSize = fmin(tmp_width, tmp_height-menuBarOffset)/8;
  bool isWider = tmp_width > tmp_height-menuBarOffset;

  if ( isWider )
  {
    offsetX = offset;
    offsetY = menuBarOffset;
  }
  else
  {
    offsetX = 0.0;
    offsetY = menuBarOffset + offset;
  }
  boardX = offsetX;
  boardY = offsetY;

  painter->setPen(QPen(QBrush(), 0));

  for ( int r = POS_MAX; r >= 0; r--)
  {
    for (int c = 0; c <= POS_MAX; c++)
    {
      if ( r % 2 == 0)
      {
        if ( c % 2 == 0)
          painter->setBrush(Qt::darkCyan);
        else
          painter->setBrush(Qt::darkGray);
      }
      else
      {
        if ( c % 2 == 1)
          painter->setBrush(Qt::darkCyan);
        else
          painter->setBrush(Qt::darkGray);
      }

      if ( selectedPosition().x == c && selectedPosition().y == r )
      {
        painter->setBrush(Qt::darkYellow);
      }

      painter->drawRect(QRect(boardX, boardY,  sqrSize, sqrSize));
      boardX+=sqrSize;
    }
    boardY+=sqrSize;
    boardX = offsetX;
  }

}

void ChessGUI::drawPieces(QPainter *painter)
{
  vector<Piece *> board;
  getBoard(&board);

  // Board pieces display
  QDir dir(".");
  QString imgPath = dir.absolutePath();
  imgPath += "/.src/chess_set.png";

  //printf("%s\r\n", imgPath.toStdString().c_str());

  QImage piecesPng(imgPath);

  int boardBottom = offsetY + sqrSize * 8;
  for (int8_t i = 0; i < (int8_t)board.size(); i++)
  {
    int type = board.at(i)->getType();

    QRect onBoard(offsetX + sqrSize * board.at(i)->position().x,
                  boardBottom - sqrSize - sqrSize * board.at(i)->position().y,
                  sqrSize, sqrSize);

    int pStartX = piecesPng.size().width()/5.74 * type;
    int pStartY = board.at(i)->getColor() == CPL_WHITE ? piecesPng.size().height()/2 : 0;

    QRect onPiecesImg(pStartX, pStartY, piecesPng.size().width()/7, piecesPng.size().height()/2);

    painter->drawImage(onBoard, piecesPng, onPiecesImg);
  }
}

void ChessGUI::drawMoves(QPainter *painter)
{

  if ( selectedIndex >= 0 )
  {
    getLegalMoves();

    if ( drawPieceValidMoves )
    {
      getMoves();

      painter->setBrush(QBrush(QColor(255,255,0,128)));
      painter->setPen(QPen(Qt::transparent));

      for ( int i = 0; i < moves_count; i++)
      {
        QPoint sqrCenter = QPoint(offsetX + sqrSize*(moves[i].x + 0.5 ), offsetY + sqrSize*(POS_MAX - moves[i].y + 0.5));
        painter->drawEllipse(sqrCenter, sqrSize/12, sqrSize/12);
      }
    }

    if ( drawLegalMoves )
    {
      painter->setBrush(QBrush(QColor(0,255,0,128)));
      painter->setPen(QPen(Qt::transparent));

      for ( int i = 0; i < legal_moves_count; i++)
      {
        QPoint sqrCenter = QPoint(offsetX + sqrSize*(legal_moves[i].x + 0.5 ), offsetY + sqrSize*(POS_MAX - legal_moves[i].y + 0.5));
        painter->drawEllipse(sqrCenter, sqrSize/6, sqrSize/6);
      }
    }
  }
}

void ChessGUI::drawThreats(QPainter *painter)
{

  if ( drawThreatMapBlack && threat_mask_black )
  {
    QBrush b = QBrush(QColor(0,0,255,100));
    QPen p = QPen(Qt::transparent);

    painter->setBrush(b);
    painter->setPen(p);

    for ( int8_t b = 0; b < 64; b++)
    {
      if ( threat_mask_black & (uint64_t)1 << (b))
      {
        int8_t file = b / 8;
        int8_t rank = b % 8;
        //printf("(%d:%d) ", rank, file);
        painter->drawRect(offsetX + sqrSize * rank, offsetY + sqrSize*(7-file), sqrSize, sqrSize);

      }
    }
    //printf("\n\r");
  }

  if ( drawThreatMapWhite && threat_mask_white )
  {
    QBrush b = QBrush(QColor(255,0,0,100));
    QPen p = QPen(Qt::transparent);

    painter->setBrush(b);
    painter->setPen(p);

    for ( int8_t b = 0; b < 64; b++)
    {
      if ( threat_mask_white & (uint64_t)1 << (b))
      {
        int8_t file = b / 8;
        int8_t rank = b % 8;
        //printf("(%d:%d) ", rank, file);
        painter->drawRect(offsetX + sqrSize * rank, offsetY + sqrSize*(7-file), sqrSize, sqrSize);

      }
    }
    //printf("\n\r");
  }

  if ( signalChecks )
  {
    if ( isCheck() )
    {

      painter->setBrush(QBrush(QColor(255,0,0)));
      painter->setPen(QPen(Qt::transparent));

      ChessPosition kPos = getKingPosition();

      painter->drawRect(offsetX + sqrSize * kPos.x, offsetY + sqrSize*(7-kPos.y), sqrSize, sqrSize);
    }
  }
}

// ---- ---- ---- ---- Menu options ---- ---- ---- ----
void ChessGUI::on_actionThreat_Map_White_toggled(bool checked)
{
  drawThreatMapWhite = checked;
  repaint();
}

void ChessGUI::on_actionThreat_Map_Black_toggled(bool checked)
{
  drawThreatMapBlack = checked;
  repaint();

}

void ChessGUI::on_actionPiece_Legal_Moves_toggled(bool checked)
{
  drawLegalMoves = checked;
  repaint();
}

void ChessGUI::on_actionPiece_moves_toggled(bool checked)
{
  drawPieceValidMoves = checked;
  repaint();
}

void ChessGUI::on_actionNew_game_triggered()
{
  gameOver = false;
  startGame();
  repaint();
}



// ---- ---- ---- ---- Examples Menu options handle ---- ---- ---- ----

void ChessGUI::on_actionEn_Passant_triggered()
{
  loadExcample(1);
  repaint();
  gameOver = false;
}


void ChessGUI::on_actionEn_passant_triggered()
{
  gameOver = false;
  loadExcample(2);
  repaint();
}


void ChessGUI::on_actionCheck_triggered()
{
  gameOver = false;
  loadExcample(3);
  repaint();
}


void ChessGUI::on_actionCheck_mate_triggered()
{
  gameOver = false;
  loadExcample(4);
  repaint();
}

void ChessGUI::on_actionCheck_Blocking_triggered()
{
  gameOver = false;
  loadExcample(5);
  repaint();
}

void ChessGUI::on_actionCastle_triggered()
{
  gameOver = false;
  loadExcample(6);
  repaint();
}

void ChessGUI::on_actionCastle_block_triggered()
{
  gameOver = false;
  loadExcample(7);
  repaint();
}

void ChessGUI::on_actionPinned_piece_moves_triggered()
{
  gameOver = false;
  loadExcample(8);
  repaint();
}

void ChessGUI::on_actionEn_passant_pin_triggered()
{
  gameOver = false;
  loadExcample(9);
  repaint();
}




