#include "piecesmanualtest.h"

#include <iostream>
#include <stdio.h>
#include <string>
#include <string.h>


#ifdef __linux__
//#define consoleClear() system("clear")
#define consoleClear()
#else
#define consoleClear() system("cls")
#endif

#define confirmAction() cout << "Press any key to continue..."; getchar();

using namespace std;

PiecesManualTest::PiecesManualTest():
  moves(nullptr),
  moves_count(0)
{
  clearBoard();
}

PiecesManualTest::~PiecesManualTest()
{
  delete[] moves;
}

void PiecesManualTest::clearBoard()
{
  for ( int row = 0; row < 8; row++)
  {
    for(int col = 0; col < 8; col++)
    {
      board[row][col] = '-';
    }
  }
}

void PiecesManualTest::updateBoard()
{
  for (int move = 0; move < moves_count; move++)
  {
    board[moves[move].x][moves[move].y] = 'x';
  }
}

void PiecesManualTest::printBoard()
{
  for ( int row = POS_MAX; row >= 0; row--)
    {
      for(int col = 0; col <= POS_MAX; col++)
      {
        cout << board[row][col] << ' ';
      }
      cout << endl;
    }
}

void PiecesManualTest::printRank(uint8_t rank)
{
    for(int col = 0; col < 8; col++)
    {
      cout << board[rank][col] << ' ';
    }
}


void PiecesManualTest::printMovesCount(E_PIECE_TYPE piece)
{

  for ( int rank_pos = 0; rank_pos < 8; rank_pos++)
  {
    for ( int file_pos = 0; file_pos < 8; file_pos++)
    {
      switch ( piece )
      {
      case P_KING:
        King(ChessPosition(rank_pos, file_pos)).getMoves(&moves, &moves_count);
        board[rank_pos][file_pos] = CN_KING;
        break;
      case P_QUEEN:
        Queen(ChessPosition(rank_pos, file_pos)).getMoves(&moves, &moves_count);
        board[rank_pos][file_pos] = CN_QUEEN;
        break;
      case P_ROOK:
        Rook(ChessPosition(rank_pos, file_pos)).getMoves(&moves, &moves_count);
        board[rank_pos][file_pos] = CN_ROOK;
        break;
      case P_KNIGHT:
        Knight(ChessPosition(rank_pos, file_pos)).getMoves(&moves, &moves_count);
        board[rank_pos][file_pos] = CN_KNIGHT;
        break;
      case P_BISHOP:
        Bishop(ChessPosition(rank_pos, file_pos)).getMoves(&moves, &moves_count);
        board[rank_pos][file_pos] = CN_BISHOP;
        break;
      default:
        return;
      }
      printf("%2d ", moves_count);
    }
    cout << endl;
  }

}

void PiecesManualTest::testPiece(E_PIECE_TYPE piece)
{
  string piecename;

  switch ( piece )
  {
  case P_KING:
    piecename = "King";
    break;
  case P_QUEEN:
    piecename = "Queen";
    break;
  case P_ROOK:
    piecename = "Rook";
    break;
  case P_KNIGHT:
    piecename = "Knight";
    break;
  case P_BISHOP:
    piecename = "Bishop";
    break;
  default:
    return;
  }

  printf("%s possible moves count:\n\r", piecename.c_str());
  printMovesCount(piece);
  confirmAction();
  consoleClear();

  printf("%s possible moves map:\n\r", piecename.c_str());
  for ( int rank_pos = 0; rank_pos < 8; rank_pos++)
  {
    for ( int printed_rank = 7; printed_rank >= 0; printed_rank--)
    {
      for ( int file_pos = 0; file_pos < 8; file_pos++)
      {
        clearBoard();

        switch ( piece )
        {
        case P_KING:
          King(ChessPosition(rank_pos, file_pos)).getMoves(&moves, &moves_count);
          board[rank_pos][file_pos] = CN_KING;
          break;
        case P_QUEEN:
          Queen(ChessPosition(rank_pos, file_pos)).getMoves(&moves, &moves_count);
          board[rank_pos][file_pos] = CN_QUEEN;
          break;
        case P_ROOK:
          Rook(ChessPosition(rank_pos, file_pos)).getMoves(&moves, &moves_count);
          board[rank_pos][file_pos] = CN_ROOK;
          break;
        case P_KNIGHT:
          Knight(ChessPosition(rank_pos, file_pos)).getMoves(&moves, &moves_count);
          board[rank_pos][file_pos] = CN_KNIGHT;
          break;
        case P_BISHOP:
          Bishop(ChessPosition(rank_pos, file_pos)).getMoves(&moves, &moves_count);
          board[rank_pos][file_pos] = CN_BISHOP;
          break;
        default:
          return;
        }

        for ( int i = 0; i < moves_count; i++)
        {
          board[moves[i].x][moves[i].y] = 'x';
        }

        printRank(printed_rank);
        cout << "  ";
      }
      cout << endl;
    }
    cout << endl;
  }

  confirmAction();
  consoleClear();
}

void PiecesManualTest::run()
{
  testPiece(P_KING);
  testPiece(P_QUEEN);
  testPiece(P_ROOK);
  testPiece(P_BISHOP);
  testPiece(P_KNIGHT);
  //testPiece(P_PAWN); todo
}
