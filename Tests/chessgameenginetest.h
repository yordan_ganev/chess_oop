#ifndef CHESSGAMEENGINETEST_H
#define CHESSGAMEENGINETEST_H

#include "../Chess/inc/chessgameengine.h"

class ChessGameEngineTest : public ChessGameEngine
{
private:
  //ChessGameEngine game;

public:
  ChessGameEngineTest();

  void printBoard();
  void run();
};

#endif // CHESSGAMEENGINETEST_H
