#include "../Tests/chessgameenginetest.h"
#include "../Chess/inc/chessgameengine.h"

#include "../Pieces/inc/piece.h"

#include <stdint.h>
#include <iostream>
#include <vector>

#define CLEAR_CONSOLE_AFTER_TEST 0

#if CLEAR_CONSOLE_AFTER_TEST
#ifdef __linux__
#define consoleClear() system("clear")
#else
#define consoleClear() system("cls")
#endif
#else
#define consoleClear()
#endif

#define confirmAction() cout << "Press any key to continue..."; getchar(); consoleClear()

#define print_expr(expr) cout << #expr << " = " << (int)expr << endl

int _tmp_test;
#define print_test(expr, expect) _tmp_test = (int)expr;\
  cout << #expr << " = " << _tmp_test << "\t [TEST " <<  ((_tmp_test == expect) ? "PASSED!]" : "FAILED!]") << endl

ChessGameEngineTest::ChessGameEngineTest() :
  ChessGameEngine()
{
}

using namespace std;

void ChessGameEngineTest::printBoard()
{
  vector<Piece *> board;

  getBoard(&board);

  for(int8_t r = 7; r >= 0; r--)
  {
    for (int8_t c = 0; c < 8; c++)
    {
      bool found = 0;
      for (int8_t i = 0; i < (int8_t)board.size(); i++)
      {
        if ( board.at(i)->position().x == c && board.at(i)->position().y == r)
        {

          found = 1;

          switch (board.at(i)->getType())
          {
            case P_KING:
              cout << ((board.at(i)->getColor() == CPL_WHITE) ? CN_KING : (char)tolower(CN_KING))<< " ";
              break;
            case P_QUEEN:
              cout << ((board.at(i)->getColor() == CPL_WHITE) ? CN_QUEEN : (char)tolower(CN_QUEEN)) << " ";
              break;
            case P_BISHOP:
              cout << ((board.at(i)->getColor() == CPL_WHITE) ? CN_BISHOP : (char)tolower(CN_BISHOP)) << " ";
              break;
            case P_KNIGHT:
              cout << ((board.at(i)->getColor() == CPL_WHITE) ? CN_KNIGHT : (char)tolower(CN_KNIGHT))<< " ";
              break;
            case P_ROOK:
              cout << ((board.at(i)->getColor() == CPL_WHITE) ? CN_ROOK : (char)tolower(CN_ROOK)) << " ";
              break;
            case P_PAWN:
              cout << ((board.at(i)->getColor() == CPL_WHITE) ? "P " : "p ");
              break;
          }
          break;
        }
      }

      if (!found)
      {
        cout << "- ";
      }
    }
    cout << endl;
  }
}

void ChessGameEngineTest::run()
{
/*
  // Basic
  cout << "Start Game!" << endl;
  startGame();

  cout << endl;
  printBoard();

  move(0, 1, 0, 3);

  cout << "move(0, 1, 0, 3)" << endl;
  printBoard();

  select(0, 6);
  move(ChessPosition(0, 4));

  cout << "select(0, 6)" << endl;
  cout << "move(ChessPosition(0, 4))" << endl;
  printBoard();

  // Todo test capture
  move(3, 0, 3, 7);
  cout << "move(0, 3, 7, 3) test caputre" << endl;
  printBoard();

  // ---- Sequence 1 ----
  print_expr(loadExcample(1));

  printBoard();
*/

  // ---- Sequence 2 ----
  // En passant, capture, promotion
  print_expr(loadExcample(2));
  printBoard();
  print_test(move(5, 1, 5, 3), ENGINE_OK);
  printBoard();
  print_test(move(4, 3, 5, 2), ENGINE_OK);
  printBoard();
  print_test(move(1, 0, 0, 2), ENGINE_OK);
  printBoard();
  print_test(move(5, 2, 6, 1), ENGINE_OK);
  printBoard();
  print_test(move(6, 0, 7, 2), ENGINE_OK);
  printBoard();
  print_test(move(6, 1, 7, 0), ENGINE_OK);
  printBoard();
  confirmAction();

  // ---- Sequence 3 ----
  // Checkmate
  print_expr(loadExcample(3));

  printBoard();
  print_test(isCheck(), false);
  print_test(isCheckMate(), false);

  print_test(move(7, 4, 5, 6), ENGINE_OK);
  printBoard();

  print_test(isCheck(), true);
  print_test(isCheckMate(), true);

  confirmAction();

  // ---- Sequence 4 ----
  // Draw
  print_expr(loadExcample(4));
  printBoard();
  print_test(isDraw(), false);
  print_test(move(6, 0, 6, 5), ENGINE_OK);
  printBoard();
  print_test(isDraw(), true);
  confirmAction();


  // ---- Sequence 5 ----
  // Check block, capture
  print_expr(loadExcample(5));
  printBoard();

  print_test(move(1,1,1,3), ENGINE_OK);
  printBoard();

  print_test(move(0,4,1,3), ENGINE_OK);
  printBoard();

  print_test(move(3,0,3,1), ENGINE_OK);
  printBoard();
  confirmAction();

  // ---- Sequence 6 ----
  // Castle short and long side
  print_expr(loadExcample(6));
  printBoard();

  print_test(select(4, 0), ENGINE_OK);
  print_test(getLegalMoves(), ENGINE_OK);

  for ( int i = 0; i < legal_moves_count; i++)
    cout << "(" << (int)legal_moves[i].x << "," << (int)legal_moves[i].y << ") ";
  cout << endl;

  print_test(move(ChessPosition(7,0)), ENGINE_OK);

  printBoard();

  print_test(select(4, 7), ENGINE_OK);
  print_test(getLegalMoves(), ENGINE_OK);

  for ( int i = 0; i < legal_moves_count; i++)
    cout << "(" << (int)legal_moves[i].x << "," << (int)legal_moves[i].y << ") ";
  cout << endl;
  print_test(move(ChessPosition(0,7)), ENGINE_OK);
  printBoard();

  confirmAction();

  // ---- Sequence 7 ----
  // Blocked castle
  print_expr(loadExcample(7));
  printBoard();

  vector<Piece *> board;
  getBoard(&board);

  print_test(select(0, 0), ENGINE_OK);
  print_test(((Rook *)(board.at(getSelectedIndex())))->hasMoved(), false);

  print_test(select(0, 7), ENGINE_OK);
  print_test(((Rook *)(board.at(getSelectedIndex())))->hasMoved(), false);

  print_test(select(4, 0), ENGINE_OK);
  print_test(((King *)(board.at(getSelectedIndex())))->hasMoved(), false);

  print_test(getLegalMoves(), ENGINE_OK);

  for ( int i = 0; i < legal_moves_count; i++)
    cout << "(" << (int)legal_moves[i].x << "," << (int)legal_moves[i].y << ") ";
  cout << endl;

  print_test(move(ChessPosition(4,1)), ENGINE_OK);
  confirmAction();

  // ---- Sequence 8 ----
  // Calculate thread map legal king legal moves
  print_expr(loadExcample(7));
  printBoard();

  print_test( move(0,0,0,7), ENGINE_OK);
  printBoard();

  print_test( select(4, 7), ENGINE_OK);

  getLegalMoves();

  for ( int i = 0; i < legal_moves_count; i++)
    cout << "(" << (int)legal_moves[i].x << "," << (int)legal_moves[i].y << ") ";
  cout << endl;

  confirmAction();

  // ---- Sequence 9 ----
  print_expr(loadExcample(9));
  printBoard();

  print_test( move(4,1,4,3) ,ENGINE_OK);

  printBoard();

  print_test(select(3,3), ENGINE_OK);

  getLegalMoves();


  for ( int i = 0; i < legal_moves_count; i++)
    cout << "(" << (int)legal_moves[i].x << "," << (int)legal_moves[i].y << ") ";
  cout << endl;

  confirmAction();
}
