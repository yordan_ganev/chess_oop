#ifndef PIECESMANUALTEST_H
#define PIECESMANUALTEST_H

#include <stdint.h>

// Function and definitions
#include "../Pieces/inc/piece.h"

// Classes fucntionality testing
#include "../Chess/inc/chess_position.h"
#include "../Pieces/inc/king.h"
#include "../Pieces/inc/queen.h"
#include "../Pieces/inc/rook.h"
#include "../Pieces/inc/bishop.h"
#include "../Pieces/inc/knight.h"

class PiecesManualTest : public King, Queen, Rook, Knight, Bishop
{
protected:
  char board[8][8];
  ChessPosition *moves;
  uint16_t moves_count;

public:
  PiecesManualTest();
  ~PiecesManualTest();

  void clearBoard();
  void updateBoard();
  void printBoard();


  void printRank(uint8_t rank);

  void printBoardsRank(uint8_t rank);
  void printBoardsFile(uint8_t file);

  void printMovesCount(E_PIECE_TYPE piece);

  void testPiece(E_PIECE_TYPE piece);

  void run();
};

#endif // PIECESMANUALTEST_H
