#include "../Tests/chesspositiontest.h"
#include "../Chess/inc/chess_position.h"

#include <stdint.h>
#include <iostream>
#include <vector>

using namespace std;

#define CLEAR_CONSOLE_AFTER_TEST 0

#if CLEAR_CONSOLE_AFTER_TEST
#ifdef __linux__
#define consoleClear() system("clear")
#else
#define consoleClear() system("cls")
#endif
#else
#define consoleClear()
#endif

#define confirmAction() cout << "Press any key to continue..."; getchar();

#define print_test(expr, expect) cout << #expr << " = " << (int)expr << "\t [TEST " <<  ((expr == expect) ? "PASSED!]" : "FAILED!]") << endl

void ChessPositionTest::run()
{
  cout << "Chess position tests!" << endl;

  ChessPosition default_pos = ChessPosition();
  print_test(isValidPosition(default_pos), false);

  confirmAction();
  consoleClear();

  ChessPosition valid_pos = ChessPosition(1,5);
  print_test(isValidPosition(valid_pos), true);

  confirmAction();
  consoleClear();

  ChessPosition inv_pos = ChessPosition(-2,5);
  print_test(isValidPosition(inv_pos), false);

  confirmAction();
  consoleClear();

  print_test(isValidPosition(1,5), true);

  confirmAction();
  consoleClear();

  print_test(isValidPosition(POS_MIN,POS_MIN), true);

  confirmAction();
  consoleClear();

  print_test(isValidPosition(POS_MAX,POS_MAX), true);

  confirmAction();
  consoleClear();

  print_test(isValidPosition(-2,5), false);

  confirmAction();
  consoleClear();
}
