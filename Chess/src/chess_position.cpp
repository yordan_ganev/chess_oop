#include "../inc/chess_position.h"

ChessPosition::ChessPosition() :
  x(POS_UNDEFINED), y(POS_UNDEFINED)
{}

ChessPosition::ChessPosition(int8_t x, int8_t y) :
  x(x), y(y)
{
  if (!isValidPosition(this))
  {
    x = POS_UNDEFINED;
    y = POS_UNDEFINED;
  }
}


uint8_t ChessPosition::set(int8_t x, int8_t y)
{
  if (isValidPosition(x, y))
  {
    this->x=x;
    this->y=y;
    return 1;
  }

  return 0;
}
/**
 * @brief ChessPosition::set
 * @param x
 * @param y
 * @return
 */
/*
uint8_t ChessPosition::set(const int8_t& x, const int8_t& y)
{
  if (isValidPosition(x, y))
  {
    this->x=x;
    this->y=y;
    return 1;
  }
  return 0;
}
*/

/**
 * @brief Clear position
 */
void ChessPosition::clear()
{
  x = POS_UNDEFINED;
  y = POS_UNDEFINED;
}

/**
 * @brief isValidPosition
 * @param pos
 * @return 1 if valid
 *         0 if not
 */
uint8_t isValidPosition(const int8_t& pos)
{
  return (pos >= POS_MIN && pos <= POS_MAX);
}

/**
 * @brief isLegalPosition
 * @param pos ref to Position object
 * @return 1 if valid
 *         0 if not
 */
uint8_t isValidPosition(const ChessPosition& pos)
{
  return (isValidPosition(pos.x) && isValidPosition(pos.y));
}

/**
 * @brief isLegalPosition
 * @param pos pointer to Position object
 * @return 1 if valid
 *         0 if not
 */
uint8_t isValidPosition(const ChessPosition* pos)
{
  return (isValidPosition(pos->x) && isValidPosition(pos->y));
}


/**
 * @brief isValidPosition
 * @param x
 * @param y
 * @return
 */
uint8_t isValidPosition(const int8_t& x, const int8_t& y)
{
  return (isValidPosition(x) && isValidPosition(y));
}

/**
 * @brief getMovesDist
 * @param pos
 * @param dist
 * @param pattern
 * @param moves_dest
 * @param size_dest
 * @return Check move_status enum
 */
uint8_t getMovesDist(const ChessPosition& pos, int8_t dist, uint8_t pattern, ChessPosition *moves_dest, uint16_t *size_dest)
{
  if ( !isValidPosition(pos) )
  {
    return MOVE_ERR_INV_POS;
  }

  if ( !pattern )
  {
    return MOVE_ERR_INV_PATTERN;
  }

  if ( !moves_dest || !size_dest )
  {
    return MOVE_ERR_DEST_NULLPTR;
  }

  int size = 0;

  struct _position {
    int8_t x;
    int8_t y;
  };

  int8_t x;
  int8_t y;

  if ( pattern & M_HOR_VER )
  {
    // Check clockwise (starting 12 o'clock)
    _position delta[4] = {
      {0, dist},                // top
      {dist, 0},                // right
      {0, (int8_t)-dist},       // bot
      {(int8_t)-dist, 0},       // left
    };

    for (int i = 0; i<4;i++)
    {
      x = pos.x + delta[i].x;
      y = pos.y + delta[i].y;
      if ( isValidPosition(x, y))
      {
        moves_dest[size++].set(x, y);
      }
    }
  }

  if ( pattern & M_DIAGONAL )
  {

    // Check clockwise (starting 12 o'clock)
    // todo Define f (int8_t) to format better..
    _position delta[4] = {
      {dist, dist},                       // top-right
      {dist, (int8_t)-dist},              // bot-right
      {(int8_t)-dist, (int8_t)-dist},     // bot-left
      {(int8_t)-dist, dist},              // top-left
    };

    for (int i = 0; i<4; i++)
    {
      x = pos.x + delta[i].x;
      y = pos.y + delta[i].y;
      if ( isValidPosition(x, y))
      {
        moves_dest[size++].set(x, y);
      }
    }

  }

  *size_dest = size;

  return MOVE_OK;
}
