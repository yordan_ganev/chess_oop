// Chess Game
#include "../inc/chessgameengine.h"
#include "../inc/chess_position.h"

// Chess Piece
#include "../../Pieces/inc/piece.h"
#include "../../Pieces/inc/king.h"
#include "../../Pieces/inc/queen.h"
#include "../../Pieces/inc/rook.h"
#include "../../Pieces/inc/bishop.h"
#include "../../Pieces/inc/knight.h"
#include "../../Pieces/inc/pawn.h"

// std
#include <stdint.h>
#include <inttypes.h>
#include <vector>
#include <stdlib.h>

ChessGameEngine::ChessGameEngine() :
  en_passant(ChessPosition()),
  king_white(ChessPosition()),
  king_black(ChessPosition()),
  attacker(ChessPosition()),
  selectedIndex(-1),
  player_turn(CPL_NONE),
  moves(nullptr),
  moves_count(0),
  legal_moves(nullptr),
  legal_moves_count(0),
  threat_mask_white(0),
  threat_mask_black(0)
{
  pieces.reserve(32);
  pieces_white.reserve(16);
  pieces_black.reserve(16);
}

using namespace std;

ChessGameEngine::~ChessGameEngine()
{
  discard_select();

  while ( !pieces.empty() )
  {
    delete pieces.back();
    pieces.pop_back();
  }
}

/*
 * todo error values
 */
uint8_t ChessGameEngine::startGame()
{
  while ( !pieces.empty() )
  {
    delete pieces.back();
    pieces.pop_back();
  }

  pieces_white.clear();
  pieces_black.clear();

  discard_select();

  attacker.clear();
  en_passant.clear();

  player_turn = CPL_WHITE;

  _setupPlayer(CPL_WHITE); //Add White Pieces
  _setupPlayer(CPL_BLACK); //Add Black Pieces

  _updateThreatMask(CPL_WHITE);
  _updateThreatMask(CPL_BLACK);

  return ENGINE_OK;
}

uint8_t ChessGameEngine::loadExcample(uint8_t example_id)
{
  _setupboard(example_id);

  return 0;
}

uint8_t ChessGameEngine::_setupPlayer( uint8_t player )
{
  if ( !(player == CPL_BLACK || player == CPL_WHITE) )
  {
    return 1;
  }

  vector<Piece *> *pieces_player = player == CPL_WHITE ? &pieces_white : &pieces_black;

  int8_t rank = player == CPL_WHITE ? 0 : 7;
  int8_t kingpos = 4;
  int8_t queenpos = 3;
  int8_t pawn_rank = player == CPL_WHITE ? 1 : 6;

  King *king = new King(ChessPosition(kingpos,rank), player);
  pieces.push_back(king);
  pieces_player->push_back(king);

  if ( player == CPL_WHITE)
  {
    king_white = king->position();
  }
  else if ( player == CPL_BLACK)
  {
    king_black = king->position();
  }

  Queen *queen = new Queen(ChessPosition(queenpos,rank), player);
  pieces.push_back(queen);
  pieces_player->push_back(queen);

  Rook *rook = new Rook(ChessPosition(0,rank), player);
  pieces.push_back(rook);
  pieces_player->push_back(rook);

  rook = new Rook(ChessPosition(7,rank), player);
  pieces.push_back(rook);
  pieces_player->push_back(rook);


  Bishop *bishop = new Bishop(ChessPosition(2,rank), player);
  pieces.push_back(bishop);
  pieces_player->push_back(bishop);

  bishop = new Bishop(ChessPosition(5,rank), player);
  pieces.push_back(bishop);
  pieces_player->push_back(bishop);

  Knight *knight = new Knight(ChessPosition(1,rank), player);
  pieces.push_back(knight);
  pieces_player->push_back(knight);

  knight = new Knight(ChessPosition(6,rank), player);
  pieces.push_back(knight);
  pieces_player->push_back(knight);

  for (int8_t p = 0; p < 8; p++)
  {
    Pawn *pawn = new Pawn(ChessPosition(p,pawn_rank), player);
    pieces.push_back(pawn);
    pieces_player->push_back(pawn);
  }

  return 0;
}


uint8_t ChessGameEngine::move(int8_t x, int8_t y, int8_t dx, int8_t dy)
{
  int8_t recoverSelectedIndex = selectedIndex;

  if ( select(x, y) )
  {
    return ENGINE_ERR;
  }

  uint8_t status = move(ChessPosition(dx, dy));

  selectedIndex = recoverSelectedIndex;

  return status;
}

//todo  uint8_t move(ChessPosition pos, ChessPosition dest){}
uint8_t ChessGameEngine::move(ChessPosition position)
{
  if ( selectedIndex >= (int8_t)pieces.size() )
  {
    return 1;
  }

  if ( selectedIndex == -1 )
  {
    return 1;
  }

  uint8_t status = MOVE_ERR_INV_POS;

  uint8_t idx = selectedIndex;

  // Coordinates
  int8_t dx = position.x;
  int8_t dy = position.y;

  int8_t x = pieces.at(selectedIndex)->position().x;
  int8_t y = pieces.at(selectedIndex)->position().y;

  // Calculate
  uint8_t en_passant_move = false;
  uint8_t castle_move = false;
  uint8_t player = pieces.at(idx)->getColor();

  // ------------ Move Phase 1 - Filter unique moves  ------------
  if ( pieces.at(idx)->getType() == P_PAWN ) // Check en passant
  {
    // Checks En passant for current move
    if ( dx == en_passant.x && dy == en_passant.y )
    {
      // Handle unique move
      // captures not squere of move!
      pieces.at(idx)->move(ChessPosition(dx, dy));

      if (pieces.at(idx)->position().y == POS_MAX || pieces.at(idx)->position().y == POS_MIN)
      {

        ChessPosition pos = pieces.at(idx)->position();
        capture(pos.x, pos.y); // deletes pawn

        // create other piece
        Queen *queen = new Queen(pos, player);
        pieces.push_back(queen);

        if ( player_turn == CPL_WHITE )
        {
          pieces_white.push_back(queen);
        }
        else
        {
          pieces_black.push_back(queen);
        }
      }

      capture(dx, y);

      if ( idx == (int8_t)pieces.size() )
      {
        // Piece capturing had last index and pieces are one less
        idx--;
        selectedIndex--;
      }

      en_passant_move = true;
    }

    en_passant.clear();

    // Setup en passant move for next turn
    if ( ((Pawn *)pieces.at(idx))->hasMoved() == 0 )
    {
      if ( abs(y - dy) == 2 ) // current turn - pawn has moved 2 squares forward
      {
        if ( player == CPL_WHITE)
        {
          en_passant.set(x, y+1);
          void setSelectedIndex(int8_t newSelectedIndex);
        }
        else
        {
          en_passant.set(x, y-1);
        }
      }
    }
  }
  else if ( pieces.at(idx)->getType() == P_KING ) // Check castle
  {
    en_passant.clear();

    // Get the rook
    Piece *rook;
    if ( _getPieceAt(dx, dy, &rook) == ENGINE_OK)
    {
      if ( rook->getType() == P_ROOK && rook->getColor() == player_turn )
      {
        int8_t kingX = pieces.at(idx)->position().x;
        int8_t kingY = pieces.at(idx)->position().y;

        int8_t dir = (kingX > dx) ? -1 : 1; //calculate queen side or king side

        // Set positions of pieces and update king position in engine
        rook->move(ChessPosition(kingX + (1*dir), kingY));

        ChessPosition *king_pos = (player_turn == CPL_WHITE) ? &king_white : &king_black;
        *king_pos = ChessPosition(kingX + (2*dir), kingY);
        pieces.at(idx)->move(*king_pos);

        castle_move = true;
      }
    }
  }
  else
  {
    en_passant.clear();
  }


  // ------------ Move Phase 2 - Move or skip if move was unique ------------
  if ( !en_passant_move && !castle_move ) // regular move
  {

    status = pieces.at(idx)->move(ChessPosition(dx, dy));

    if ( pieces.at(idx)->getType() == P_KING )
    {
      if (player_turn == CPL_WHITE)
      {
        king_white = ChessPosition(dx, dy);
      }
      else
      {
        king_black = ChessPosition(dx, dy);
      }
    }

    if (status != MOVE_OK)
    {
      return status;
    }

    capture(dx, dy);

    if ( idx == (int8_t)pieces.size() )
    {
      // Piece capturing had last index and pieces are one less
      idx--;
      selectedIndex--;
    }

    setNextToPlay( !player_turn );

    // Check for promotion
    if ( pieces.at(idx)->getType() == P_PAWN && (pieces.at(idx)->position().y == POS_MAX || pieces.at(idx)->position().y == POS_MIN))
    {
      ChessPosition pos = pieces.at(idx)->position();

      capture(pos.x, pos.y); // deletes pawn

      // create other piece
      Queen *queen = new Queen(pos, player);
      pieces.push_back(queen);

      if ( player == CPL_WHITE )
      {
        pieces_white.push_back(queen);
      }
      else
      {
        pieces_black.push_back(queen);
      }
    }

    status = MOVE_OK;
  }
  else
  {
    setNextToPlay( !player_turn );
  }

  attacker.clear();

  // Move Last Phase
  _updateThreatMask(CPL_WHITE);
  _updateThreatMask(CPL_BLACK);

  return status;
}

uint8_t ChessGameEngine::capture(int8_t x, int8_t y)
{
  vector<Piece *> *pieces_split = (player_turn == CPL_WHITE) ? &pieces_black : &pieces_white;

  int8_t index_split = -1;
  int8_t index_group = -1;


  for (int8_t i = 0; i < (int8_t)pieces_split->size(); i++)
  {
    if ( pieces_split->at(i)->position().x == x && pieces_split->at(i)->position().y == y)
    {
      index_split = i;
      break;
    }
  }

  if ( index_split != -1 )
  {
    for (int8_t i = 0; i < (int8_t)pieces.size(); i++)
    {
      if ( pieces.at(i)->position().x == x && pieces.at(i)->position().y == y)
      {
        if ( pieces.at(i)->getColor() != player_turn)
        {
          delete pieces.at(i);

          pieces.erase(pieces.begin()+i);
          pieces_split->erase(pieces_split->begin()+index_split);

          return ENGINE_OK;
        }
      }
    }
  }

  return ENGINE_ERR;
}

uint8_t ChessGameEngine::getMoves(int8_t x, int8_t y, uint8_t player)
{
  vector<Piece *> piece = player == CPL_WHITE ? pieces_white : pieces_black;

  for (int8_t i = 0; i < (int8_t)piece.size(); i++)
  {
    if ( piece.at(i)->position().x == x && piece.at(i)->position().y == y)
    {
      return piece.at(i)->getMoves(&moves, &moves_count);
    }
  }

  return ENGINE_ERR;
}

uint8_t ChessGameEngine::getMoves(int8_t x, int8_t y)
{
  return getMoves(ChessPosition(x, y));
}

uint8_t ChessGameEngine::getMoves(ChessPosition pos)
{
  for (int8_t i = 0; i < (int8_t)pieces.size(); i++)
  {
    if ( pieces.at(i)->position().x == pos.x && pieces.at(i)->position().y == pos.y)
    {
      return pieces.at(i)->getMoves(&moves, &moves_count);
    }
  }

  return ENGINE_ERR;
}

uint8_t ChessGameEngine::getMoves()
{
  if ( selectedIndex >= (int8_t)pieces.size() || selectedIndex < (int8_t)-1)
  {
    return ENGINE_ERR;
  }

  return pieces.at(selectedIndex)->getMoves(&moves, &moves_count);

}

uint8_t ChessGameEngine::getType(int8_t x, int8_t y)
{
  for (int8_t i = 0; i < (int8_t)pieces.size(); i++)
  {
    if ( pieces.at(i)->position().x == x && pieces.at(i)->position().y == y) {
      return pieces.at(i)->getType();
    }
  }

  return P_NONE;
}

uint8_t ChessGameEngine::getType(ChessPosition pos)
{
  return getType(pos.x, pos.y);
}

uint8_t ChessGameEngine::getColor(int8_t x, int8_t y)
{
  for (int8_t i = 0; i < (int8_t)pieces.size(); i++)
  {
    if ( pieces.at(i)->position().x == x && pieces.at(i)->position().y == y)
    {
      return pieces.at(i)->getColor();
    }
  }
  return CPL_NONE;
}

uint8_t ChessGameEngine::getColor(ChessPosition pos)
{
  return getColor(pos.x, pos.y);
}

uint8_t ChessGameEngine::select(int8_t x, int8_t y)
{
  for (int8_t i = 0; i < (int8_t)pieces.size(); i++)
  {
    if ( pieces.at(i)->position().x == x && pieces.at(i)->position().y == y) {
      selectedIndex = i;
      return ENGINE_OK;
    }
  }

  return ENGINE_ERR;
}

uint8_t ChessGameEngine::select(ChessPosition pos)
{
  for (int8_t i = 0; i < (int8_t)pieces.size(); i++)
  {
    if ( pieces.at(i)->position().x == pos.x && pieces.at(i)->position().y == pos.y)
    {
      selectedIndex = i;
      return ENGINE_OK;
    }
  }

  return ENGINE_ERR;
}

uint8_t ChessGameEngine::discard_select()
{
  selectedIndex = -1;

  delete[] moves;
  delete[] legal_moves;

  moves = nullptr;
  legal_moves = nullptr;

  legal_moves_count = 0;
  moves_count = 0;

  return ENGINE_OK;
}

uint8_t ChessGameEngine::getBoard(vector<Piece *> *dest)
{
  return getPieces(dest);
}

uint8_t ChessGameEngine::getPieces(std::vector<Piece *> *dest, uint8_t player)
{
  if ( player != CPL_WHITE && player != CPL_BLACK && player != CPL_NONE)
    return ENGINE_ERR;

  if ( player == CPL_NONE )
  {
    *dest = pieces;
    return ENGINE_OK;
  }
  else if ( player == CPL_WHITE )
  {
    *dest = pieces_white;
    return ENGINE_OK;
  }
  else if ( player == CPL_BLACK )
  {
    *dest = pieces_black;
    return ENGINE_OK;
  }

  return ENGINE_ERR;
}

uint8_t ChessGameEngine::nextToPlay()
{
  return player_turn;
}

void ChessGameEngine::setNextToPlay(int8_t player_turn)
{
  this->player_turn = player_turn;
}

int8_t ChessGameEngine::getSelectedIndex()
{
  return selectedIndex;
}

void ChessGameEngine::setSelectedIndex(int8_t newSelectedIndex)
{
  discard_select();

  selectedIndex = newSelectedIndex;
}

ChessPosition ChessGameEngine::selectedPosition()
{
  if ( selectedIndex > (int8_t)pieces.size() || selectedIndex < 0 )
  {
    return ChessPosition();
  }
  return pieces.at(selectedIndex)->position();
}

ChessPosition ChessGameEngine::getKingPosition()
{
  ChessPosition result;
  if ( player_turn == CPL_WHITE )
  {
    result  = king_white;
  }
  else if ( player_turn == CPL_NONE )
  {
    result = ChessPosition();
  }
  else
  {
    result  = king_black;
  }

  return result;
}

uint8_t ChessGameEngine::isCheckMate()
{
  if ( !isCheck() )
  {
    return false;
  }

  discard_select();
  attacker.clear();

  ChessPosition* king_pos = (player_turn == CPL_WHITE) ? &king_white : &king_black;


  // Get count of attackers
  uint8_t attackers_count = 0;

  for( uint8_t i=0; i < pieces.size(); i++ )
  {
    if ( pieces.at(i)->getColor() != player_turn )
    {
      selectedIndex = i;
      _calcLegalMoves();

      for ( int i = 0; i < legal_moves_count; i++)
      {
        if ( king_pos->x == legal_moves[i].x && king_pos->y == legal_moves[i].y)
        {
          attacker.x = pieces.at(selectedIndex)->position().x;
          attacker.y = pieces.at(selectedIndex)->position().y;

          if ( attackers_count )
          {
            attacker.clear();
            attackers_count++;
            break;
          }

          attackers_count++;
        }

      }
    }
  }

  uint8_t status = false;

  uint8_t hasMoves = false;

  // If king is attacked by only 1 piece you can move king,
  // block the attack or capture the attacking piece
  if (attackers_count == 1)
  {
    for( uint8_t i=0; i < pieces.size(); i++ )
    {
      if (pieces.at(i)->getColor() == player_turn)
      {
        selectedIndex = i;
        _calcLegalMoves();

        for ( int8_t m = 0; m < legal_moves_count; m++)
        {
          if ( isValidPosition(legal_moves[m]) )
          {
            hasMoves = true;
            break;
          }
        }
      }

      if ( hasMoves )
      {
        break;
      }
    }
  }
  // If king is attacked by more than 1 piece you can move only king
  else
  {
    ChessPosition *king = (player_turn == CPL_WHITE) ? &king_white : &king_black;
    select(*king);

    getLegalMoves();

    for ( int8_t m = 0; m < legal_moves_count; m++)
    {
      if ( isValidPosition(legal_moves[m]) )
      {
        hasMoves = true;
        break;
      }
    }

  }

  if ( hasMoves == false )
  {
    status = true;
  }

  discard_select();
  return status;
}

uint8_t ChessGameEngine::isDraw()
{
  if ( isCheck() )
  {
    return false;
  }
  else if ( pieces.size() == 2)
  {
    return true; // assuming pieces left are two kings !
  }
  else
  {
    // Get all pieces of players turn
    // And check if all have no valid moves left
    for( uint8_t i=0; i < pieces.size(); i++ )
    {
      if ( pieces.at(i)->getColor() == player_turn )
      {
        selectedIndex = i;
        _calcLegalMoves();

        for ( int m = 0; m < legal_moves_count; m++)
        {
          if ( isValidPosition(legal_moves[m]) )
          {
            discard_select();
            return false;
          }
        }
      }
    }
  }

  return true;
}

uint8_t ChessGameEngine::isCheck()
{
  if ( player_turn == CPL_WHITE)
  {
    if( threat_mask_black & ( (uint64_t)1 << (8*king_white.y + king_white.x)) )
    {
      return true;
    }
  }
  else
  {
    if( threat_mask_white & ( (uint64_t)1 << (8*king_black.y + king_black.x)) )
    {
      return true;
    }
  }

  return false;
}

uint8_t ChessGameEngine::getLegalMoves()
{
  return _calcLegalMoves();;
}

uint8_t ChessGameEngine::_calcLegalMoves()
{
  pieces.at(selectedIndex)->getMoves(&moves, &moves_count);

  // Allocate mem of positions
  if ( legal_moves != nullptr )
    delete[] legal_moves; // Clear previous memory allocations

  int pre_legal_moves_count = moves_count;

  // Simplify Castle handle
  // Selected King -> Select Rook -> Castle
  // Todo: valid moves on every square between rooks and king
  if ( pieces.at(selectedIndex)->getType() == P_KING)
  {
    pre_legal_moves_count += 2; //left and right rooks of king
  }

  legal_moves = new ChessPosition[moves_count];

  legal_moves_count = 0;

  _updateLegalMoves();

  _verifyLegalMoves();

  return 0;
}

void ChessGameEngine::_updateThreatMask(uint8_t player)
{
  vector<Piece *> *pieces_player = (player == CPL_WHITE) ? &pieces_white : &pieces_black;

  if ( player == CPL_WHITE )
  {
    threat_mask_white = 0;
  }
  else if (player == CPL_BLACK)
  {
    threat_mask_black = 0;
  }

  uint64_t *threat_mask = ( player == CPL_WHITE ) ? &threat_mask_white : &threat_mask_black;

  for (uint8_t i = 0; i < pieces_player->size(); i++)
  {
    uint8_t pattern = pieces_player->at(i)->getMovePattern();

    if (pattern & M_HOR_VER)
    {
      deltaCoordinate delta[] = {
        {1, 0},
        {-1, 0},
        {0, 1},
        {0, -1}
      };

      _addThreaths(player, pieces_player->at(i)->position(), delta, sizeof(delta)/sizeof(deltaCoordinate));
    }

    if (pattern & M_DIAGONAL)
    {
      deltaCoordinate delta[] = {
        {1, 1},
        {1, -1},
        {-1, -1},
        {-1, 1}
      };

      _addThreaths(player, pieces_player->at(i)->position(), delta, sizeof(delta)/sizeof(deltaCoordinate));
    }

    if (pattern & (M_L_JUMP | M_SQUERE))
    {
      pieces_player->at(i)->getMoves(&moves, &moves_count);

      for ( int m = 0; m < moves_count; m++)
      {
        *threat_mask |= (uint64_t)1 << (8*moves[m].y + moves[m].x);
      }

    }

    if (pattern & (M_FORWARD | M_EN_PASSANT))
    {
      uint8_t player = pieces_player->at(i)->getColor();

      int8_t x = pieces_player->at(i)->position().x;
      int8_t y = pieces_player->at(i)->position().y;

      int8_t dir = (player == CPL_WHITE) ? 1 : -1;

      if ( isValidPosition(y+dir) )
      {

        // right side capture
        if ( isValidPosition(x+1) )
        {
            *threat_mask |= (uint64_t)1 << (8*(dir+y) + x+1);
        }

        // left side capture
        if ( isValidPosition(x-1) )
        {
          *threat_mask |= (uint64_t)1 << (8*(dir+y) + x-1);
        }
      }
    }

  }

}

void ChessGameEngine::_updateLegalMoves()
{
  uint8_t pattern = pieces.at(selectedIndex)->getMovePattern();

  if (pattern & M_HOR_VER)
  {
    deltaCoordinate delta[] = {
      {1, 0},
      {-1, 0},
      {0, 1},
      {0, -1}
    };

    _addLegalMoves(delta, sizeof(delta)/sizeof(deltaCoordinate));
  }

  if (pattern & M_DIAGONAL)
  {
    deltaCoordinate delta[] = {
      {1, 1},
      {1, -1},
      {-1, -1},
      {-1, 1}
    };

    _addLegalMoves(delta, sizeof(delta)/sizeof(deltaCoordinate));
  }

  if (pattern & M_L_JUMP)
  {
    uint8_t player = pieces.at(selectedIndex)->getColor();

    for ( int m = 0; m < moves_count; m++)
    {
      if (getColor(moves[m]) != player)
      {
        legal_moves[legal_moves_count++] = moves[m];
      }
    }
  }

  if (pattern & (M_FORWARD | M_EN_PASSANT))
  {
    // HAS MOVE PATTERN OF PAWN +
    uint8_t moved = ((Pawn *)pieces.at(selectedIndex))->hasMoved();

    uint8_t player = pieces.at(selectedIndex)->getColor();

    int8_t x = pieces.at(selectedIndex)->position().x;
    int8_t y = pieces.at(selectedIndex)->position().y;

    int8_t dir = (player == CPL_WHITE) ? 1 : -1;

    uint8_t owner;

    if ( isValidPosition(y+dir) )
    {
      // Check forward on same FILE for
      if ( getColor(ChessPosition(x, y+dir)) == CPL_NONE )
      {
        // Can move forward 1 rank
        legal_moves[legal_moves_count++] = ChessPosition(x, y+dir);

        // Check forward on same FILE for 2 ranks forward
        if (!moved && isValidPosition(y+dir*2))
        {
          if ( getColor(ChessPosition(x, y+dir*2)) == CPL_NONE )
          {
            // Can move forward 1 rank
            legal_moves[legal_moves_count++] = ChessPosition(x, y+dir*2);
          }
        }
      }

      // right side capture
      if ( isValidPosition(x+1) )
      {
        owner = getColor(ChessPosition(x+1, dir+y));
        if ( owner != CPL_NONE && owner != player)
        {
          legal_moves[legal_moves_count++] = ChessPosition(x+1, dir+y);
        }
        else if ( isValidPosition(en_passant) )
        {
          if (  (en_passant.x == x+1 && en_passant.y == dir+y) )
          {
            legal_moves[legal_moves_count++] = ChessPosition(x+1, dir+y);
          }
        }
      }

      // left side capture
      if ( isValidPosition(x-1) )
      {
        owner = getColor(ChessPosition(x-1, dir+y));
        if ( owner != CPL_NONE && owner != player)
        {
          legal_moves[legal_moves_count++] = ChessPosition(x-1, dir+y);
        }
        else if ( isValidPosition(en_passant) )
        {
          if (  (en_passant.x == x-1 && en_passant.y == dir+y) )
          {
            legal_moves[legal_moves_count++] = ChessPosition(x-1, dir+y);
          }
        }
      }
    }
  }

  if (pattern & M_SQUERE)
  {
    pieces.at(selectedIndex)->getMoves(&moves, &moves_count);

    uint8_t player = pieces.at(selectedIndex)->getColor();

    uint64_t *threat_mask = (player == CPL_WHITE) ? &threat_mask_black : &threat_mask_white;

    for ( int m = 0; m < moves_count; m++)
    {
      if ( !(*threat_mask & (uint64_t)1 << (8*moves[m].y + moves[m].x)) )
      {
        uint8_t owner = getColor(moves[m]);
        if ( owner == CPL_NONE || owner != player )
        {
          legal_moves[legal_moves_count++] = ChessPosition(moves[m].x, moves[m].y);
        }
      }
    }
  }

  // Check means castle is not possible - skip it right away
  if (pattern & M_CASTLE && !isCheck())
  {
    uint8_t player = pieces.at(selectedIndex)->getColor();

    uint64_t *threat_mask = (player == CPL_WHITE) ? &threat_mask_black : &threat_mask_white;

    uint8_t king = selectedIndex;


    // Assumed its king piece because it has castle move
    if ( !((King *)(pieces.at(selectedIndex)))->hasMoved() )
    {
      Piece* rook = nullptr;
      // King side castle
      if ( _getPieceAt(POS_MAX, pieces.at(king)->position().y, &rook) == ENGINE_OK )
      {
        // got piece on correct position
        if ( rook->getType() == P_ROOK )
        {
          // the piece is rook
          if ( ((Rook*)(rook))->hasMoved() == false )
          {
            int8_t rank = pieces.at(king)->position().y;

            uint8_t canCastle = true;

            // as isCheck() called before hand - skip king position
            for ( int8_t file = pieces.at(king)->position().x + 1; file <= pieces.at(king)->position().x + 2; file++)
            {
              // Can NOT Castle if for squares between king and rook
              // Enemy Piece attacks or there is any other piece
              if ( (*threat_mask & (uint64_t)1 << (8*rank + file)) || getColor(file, rank) != CPL_NONE )
              {
                canCastle = false;
                break;
              }
            }

            if ( canCastle )
            {
              legal_moves[legal_moves_count++] = rook->position();
            }
          }
        }
      }

      // Queen side castle (same as king side but switch direction)
      if ( _getPieceAt(POS_MIN, pieces.at(king)->position().y, &rook) == ENGINE_OK )
      {
        // got piece on correct position
        if ( rook->getType() == P_ROOK )
        {
          // the piece is rook
          if ( ((Rook*)(rook))->hasMoved() == false )
          {
            int8_t rank = pieces.at(king)->position().y;

            uint8_t canCastle = true;

            // as isCheck() called before hand - skip king position
            for ( int8_t file = pieces.at(king)->position().x - 1; file >= pieces.at(king)->position().x - 2; file--)
            {
              // Can NOT Castle if for squares between king and rook
              // Enemy Piece attacks or there is any other piece
              if ( (*threat_mask & (uint64_t)1 << (8*rank + file)) || getColor(file, rank) != CPL_NONE )
              {
                canCastle = false;
                break;
              }
            }

            if ( canCastle )
            {
              legal_moves[legal_moves_count++] = rook->position();
            }
          }
        }
      }
    }
  }
}

void ChessGameEngine::_addLegalMoves(deltaCoordinate *delta, uint8_t size)
{
  uint8_t player = pieces.at(selectedIndex)->getColor();

  int8_t x = pieces.at(selectedIndex)->position().x;
  int8_t y = pieces.at(selectedIndex)->position().y;

  ChessPosition position;
  uint8_t owner;

  for ( uint8_t dir = 0; dir < size; dir++ )
  {
    int8_t file = x + delta[dir].x;
    int8_t rank = y + delta[dir].y;

    // Todo mb less comaprisons if delta = 0 ?
    while (isValidPosition(rank) && isValidPosition(file))
    {
      position = ChessPosition(file, rank);
      owner = getColor(position);
      if ( owner == CPL_NONE )
      {
        legal_moves[legal_moves_count++] = position;
      }
      else
      {
        if ( owner != player )
        {
          legal_moves[legal_moves_count++] = position;
          break;
        }
        else
        {
          break;
        }
      }

      file += delta[dir].x;
      rank += delta[dir].y;
    }
  }
}

uint8_t ChessGameEngine::_checkedAfterMove(uint8_t move) {
  // presets
  ChessPosition actuallPos = pieces.at(selectedIndex)->position();

  bool recoverCaptured = false;
  ChessPosition capturedPos;
  uint8_t pattern;

  // Check if piece is capturable on move position
  Piece *piece;
  if ( _getPieceAt(legal_moves[move].x, legal_moves[move].y, &piece) == ENGINE_OK )
  {
    pattern = piece->getMovePattern();
    capturedPos = piece->position();

    // simulated capture
    piece->setMovePattern(0); //clear move pattern
    piece->setPosition(ChessPosition()); // set position out of the board
    recoverCaptured = true;
  }

  // change board
  pieces.at(selectedIndex)->setPosition(legal_moves[move]);

  _updateThreatMask(!player_turn);

  uint8_t result = isCheck();

  // recover
  pieces.at(selectedIndex)->setPosition(actuallPos);

  if ( recoverCaptured )
  {
    piece->setMovePattern(pattern);
    piece->setPosition(capturedPos);
  }

  _updateThreatMask(!player_turn);

  return result;
}

void ChessGameEngine::_verifyLegalMoves()
{
  if ( pieces.at(selectedIndex)->getType() == P_PAWN )
  {
    for ( int m=0; m < legal_moves_count; m++)
    {
      uint8_t result = false;
      // preset
      ChessPosition actuallPos = pieces.at(selectedIndex)->position();

      // Check if there is possible en passant next move
      if ( isValidPosition(en_passant) )
      {
        // change board
        pieces.at(selectedIndex)->setPosition(legal_moves[m]);

        // En passant Pawn actuall position differs for player
        int8_t deltaY = 1; // player black

        if ( player_turn == CPL_WHITE )
        {
          deltaY = -1;
        }
        // Check if current legal move is en passant
        if ( legal_moves[m].x == en_passant.x && legal_moves[m].y == en_passant.y  )
        {
          // extended check - moves and En Passant Pawn because of capture of square thats not filled
          Piece * pawnPass;

          if ( _getPieceAt(en_passant.x, en_passant.y+deltaY, &pawnPass) == ENGINE_OK )
          {
            pawnPass->setPosition(legal_moves[m]);

            _updateThreatMask(!player_turn); // Calc threat mask

            // recover positon of changed pawn
            pawnPass->setPosition(ChessPosition(en_passant.x, en_passant.y+deltaY));

            result = isCheck();
          }
          else
          {
            // shouldnt be possible to go here!
            // Todo error handler
            _updateThreatMask(!player_turn);

            result = isCheck();
          }
        }
        else
        {
          // normal check
          _updateThreatMask(!player_turn);

          result = isCheck();
        }
      }
      else
      {
        // Normal Pawn moves(forward or non en passant capture - Common with other pieces
        result = _checkedAfterMove(m);
      }

      // recover
      pieces.at(selectedIndex)->setPosition(actuallPos);

      _updateThreatMask(!player_turn);

      if ( result && !(legal_moves[m].x == attacker.x && legal_moves[m].y == attacker.y) )
      {
        legal_moves[m].clear();
      }
    }
  }
  else if ( pieces.at(selectedIndex)->getType() != P_KING )
  {
    for ( int m=0; m < legal_moves_count; m++)
    {
      if ( _checkedAfterMove(m) && !(legal_moves[m].x == attacker.x && legal_moves[m].y == attacker.y) )
      {
        legal_moves[m].clear();
      }
    }
  }
}

void ChessGameEngine::_addThreaths(uint8_t player, ChessPosition pos, deltaCoordinate *delta, uint8_t size)
{
  uint64_t threat_mask = 0;

  ChessPosition position;
  uint8_t owner;

  for ( uint8_t dir = 0; dir < size; dir++)
  {
    int8_t file = pos.x + delta[dir].x;
    int8_t rank = pos.y + delta[dir].y;

    // Todo mb less comaprisons if delta = 0 ?
    while (isValidPosition(rank) && isValidPosition(file))
    {
      position = ChessPosition(file, rank);
      owner = getColor(position);
      if ( owner == CPL_NONE )
      {
        threat_mask |= (uint64_t)1 << (8*rank + file);
      }
      else
      {
        threat_mask |= (uint64_t)1 << (8*rank + file);
        break;
      }

      file += delta[dir].x;
      rank += delta[dir].y;
    }

  }

  if ( player == CPL_WHITE )
  {
    threat_mask_white |= threat_mask;
  }
  else
  {
    threat_mask_black |= threat_mask;
  }
}

uint8_t ChessGameEngine::_getPieceAt(int8_t x, int8_t y, Piece **dest)
{
  for (int8_t i = 0; i < (int8_t)pieces.size(); i++)
  {
    if ( pieces.at(i)->position().x == x && pieces.at(i)->position().y == y) {
      *dest = pieces.at(i);
      return ENGINE_OK;
    }
  }

  return ENGINE_ERR;
}

uint8_t ChessGameEngine::_getPiece(Piece **dest)
{
  if ( selectedIndex < (int8_t)pieces.size() && selectedIndex >= 0)
  {
      *dest = pieces.at(selectedIndex);
      return ENGINE_OK;
  }

  return ENGINE_ERR;
}



/**
 * @brief ChessGameEngine::_setupboard Test cases for gui
 * @param variation index of variation
 * Note: Currently used to showcase edge cases and game examples.
 * Note: can be reworked to accept some kind of chess notations
 * > Forsyth–Edwards Notation (FEN) https://en.wikipedia.org/wiki/Forsyth%E2%80%93Edwards_Notation
 */
void ChessGameEngine::_setupboard(uint8_t variation)
{
  while ( !pieces.empty() )
  {
    delete pieces.back();
    pieces.pop_back();
  }

  pieces_white.clear();
  pieces_black.clear();

  discard_select();

  attacker.clear();
  en_passant.clear();

  player_turn = CPL_WHITE;

  King *king;
  Queen *queen;
  Rook *rook;
  Bishop *bishop;
  Knight *knight;
  Pawn *pawn;


  switch ( variation )
  {
  case 0:
  break;

  case 1:
    king_white = ChessPosition(4,0);
    king = new King(king_white, CPL_WHITE);
    pieces.push_back(king);
    pieces_white.push_back(king);

    king_black = ChessPosition(3,7);
    king = new King(king_black, CPL_BLACK);
    pieces.push_back(king);
    pieces_black.push_back(king);

    bishop = new Bishop(ChessPosition(2,2), CPL_BLACK);
    pieces.push_back(bishop);
    pieces_black.push_back(bishop);
    break;

  case 2: // En passant
    // white pieces
    _setupPlayer(CPL_WHITE);

    // black pieces
    king_black = ChessPosition(4, 7);

    king = new King(king_black, CPL_BLACK);
    queen = new Queen(ChessPosition(3,7), CPL_BLACK);
    pawn = new Pawn(ChessPosition(4, 3), CPL_BLACK);

    pieces.push_back(king);
    pieces_black.push_back(king);
    pieces.push_back(queen);
    pieces_black.push_back(queen);
    pieces.push_back(pawn);
    pieces_black.push_back(pawn);
    break;

  case 3: // Checkmate
    //white pieces
    king_white = ChessPosition(4, 0);

    king = new King(king_white, CPL_WHITE);
    queen = new Queen(ChessPosition(7,4), CPL_WHITE);
    bishop = new Bishop(ChessPosition(2, 3), CPL_WHITE);

    pieces.push_back(king);
    pieces_white.push_back(king);
    pieces.push_back(queen);
    pieces_white.push_back(queen);
    pieces.push_back(bishop);
    pieces_white.push_back(bishop);

    //black pieces
    king_black = ChessPosition(4, 7);

    king = new King(king_black, CPL_BLACK);
    queen = new Queen(ChessPosition(3,7), CPL_BLACK);
    pawn = new Pawn(ChessPosition(5, 6), CPL_BLACK);

    pieces.push_back(king);
    pieces_black.push_back(king);
    pieces.push_back(queen);
    pieces_black.push_back(queen);
    pieces.push_back(pawn);
    pieces_black.push_back(pawn);

    break;

  case 4: // Draw
    //white pieces
    king_white = ChessPosition(4, 0);

    king = new King(king_white, CPL_WHITE);
    queen = new Queen(ChessPosition(6,0), CPL_WHITE);

    pieces.push_back(king);
    pieces_white.push_back(king);

    pieces.push_back(queen);
    pieces_white.push_back(queen);

    //black pieces
    king_black = ChessPosition(7, 7);

    king = new King(king_black, CPL_BLACK);
    pieces.push_back(king);
    pieces_black.push_back(king);
    break;

  case 5: // Blocking Check

    //white pieces
    king_white = ChessPosition(4, 0);

    king = new King(king_white, CPL_WHITE);
    queen = new Queen(ChessPosition(3,0), CPL_WHITE);
    knight = new Knight(ChessPosition(1,0), CPL_WHITE);

    pieces.push_back(king);
    pieces_white.push_back(king);
    pieces.push_back(queen);
    pieces_white.push_back(queen);
    pieces.push_back(knight);
    pieces_white.push_back(knight);

    bishop = new Bishop(ChessPosition(2, 0), CPL_WHITE);
    pieces.push_back(bishop);
    pieces_white.push_back(bishop);

    bishop = new Bishop(ChessPosition(5, 0), CPL_WHITE);
    pieces.push_back(bishop);
    pieces_white.push_back(bishop);

    for (int8_t p = 1; p <= 2; p++)
    {
      Pawn *pawn = new Pawn(ChessPosition(p,1), CPL_WHITE);
      pieces.push_back(pawn);
      pieces_white.push_back(pawn);
    }

    for (int8_t p = 4; p <= 5; p++)
    {
      Pawn *pawn = new Pawn(ChessPosition(p,1), CPL_WHITE);
      pieces.push_back(pawn);
      pieces_white.push_back(pawn);
    }

    //black pieces
    king_black = ChessPosition(4, 7);

    king = new King(king_black, CPL_BLACK);
    queen = new Queen(ChessPosition(0, 4), CPL_BLACK);

    pieces.push_back(king);
    pieces_black.push_back(king);

    pieces.push_back(queen);
    pieces_black.push_back(queen);


    break;

  case 6: // Castle
    //white pieces
    king_white = ChessPosition(4, 0);

    king = new King(king_white, CPL_WHITE);
    pieces.push_back(king);
    pieces_white.push_back(king);

    rook = new Rook(ChessPosition(0, 0), CPL_WHITE);
    pieces.push_back(rook);
    pieces_white.push_back(rook);

    rook = new Rook(ChessPosition(7, 0), CPL_WHITE);
    pieces.push_back(rook);
    pieces_white.push_back(rook);

    for (int8_t p = 0; p < 8; p++)
    {
      Pawn *pawn = new Pawn(ChessPosition(p,1), CPL_WHITE);
      pieces.push_back(pawn);
      pieces_white.push_back(pawn);
    }

    //white pieces
    king_black = ChessPosition(4, 7);

    king = new King(king_black, CPL_BLACK);
    pieces.push_back(king);
    pieces_black.push_back(king);

    rook = new Rook(ChessPosition(0, 7), CPL_BLACK);
    pieces.push_back(rook);
    pieces_black.push_back(rook);

    rook = new Rook(ChessPosition(7, 7), CPL_BLACK);
    pieces.push_back(rook);
    pieces_black.push_back(rook);

    for (int8_t p = 0; p < 8; p++)
    {
      Pawn *pawn = new Pawn(ChessPosition(p,6), CPL_BLACK);
      pieces.push_back(pawn);
      pieces_black.push_back(pawn);
    }
    break;

  case 7: // Castle block
    //white pieces
    king_white = ChessPosition(4, 0);

    king = new King(king_white, CPL_WHITE);
    pieces.push_back(king);
    pieces_white.push_back(king);

    rook = new Rook(ChessPosition(0, 0), CPL_WHITE);
    pieces.push_back(rook);
    pieces_white.push_back(rook);

    rook = new Rook(ChessPosition(7, 0), CPL_WHITE);
    pieces.push_back(rook);
    pieces_white.push_back(rook);

    //black pieces
    king_black = ChessPosition(4, 7);

    king = new King(king_black, CPL_BLACK);
    pieces.push_back(king);
    pieces_black.push_back(king);

    bishop = new Bishop(ChessPosition(4, 2), CPL_BLACK);
    pieces.push_back(bishop);
    pieces_black.push_back(bishop);

    rook = new Rook(ChessPosition(0, 7), CPL_BLACK);
    pieces.push_back(rook);
    pieces_black.push_back(rook);

    rook = new Rook(ChessPosition(7, 7), CPL_BLACK);
    pieces.push_back(rook);
    pieces_black.push_back(rook);

    break;

  case 8: // Pinned piece
    //white pieces
    king_white = ChessPosition(4, 0);

    king = new King(king_white, CPL_WHITE);
    knight = new Knight(ChessPosition(2,2), CPL_WHITE);

    pieces.push_back(king);
    pieces_white.push_back(king);
    pieces.push_back(knight);
    pieces_white.push_back(knight);

    //black pieces
    king_black = ChessPosition(4, 7);

    king = new King(king_black, CPL_BLACK);
    bishop = new Bishop(ChessPosition(1, 3), CPL_BLACK);

    pieces.push_back(king);
    pieces_black.push_back(king);
    pieces.push_back(bishop);
    pieces_black.push_back(bishop);
    break;

  case 9: // En Passant Pinned

    // white pieces
    king_white = ChessPosition(4, 0);

    king = new King(king_white, CPL_WHITE);
    queen = new Queen(ChessPosition(7,3), CPL_WHITE);

    for (int8_t p = 2; p < 5; p++)
    {
      pawn = new Pawn(ChessPosition(p, 1), CPL_WHITE);
      pieces.push_back(pawn);
      pieces_white.push_back(pawn);
    }
    pieces.push_back(king);
    pieces_white.push_back(king);
    pieces.push_back(queen);
    pieces_white.push_back(queen);

    // black pieces
    king_black = ChessPosition(0, 3);

    queen = new Queen(ChessPosition(3,7), CPL_BLACK);
    king = new King(king_black, CPL_BLACK);
    pawn = new Pawn(ChessPosition(3, 3), CPL_BLACK);

    pieces.push_back(king);
    pieces_black.push_back(king);
    pieces.push_back(queen);
    pieces_black.push_back(queen);
    pieces.push_back(pawn);
    pieces_black.push_back(pawn);
    break;
  }

  _updateThreatMask(CPL_WHITE);
  _updateThreatMask(CPL_BLACK);

}
