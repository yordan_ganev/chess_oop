#ifndef CHESS_POSITION_H
#define CHESS_POSITION_H

#include "stdint.h"

#define POS_UNDEFINED     (-1)
#define POS_MIN           (0)
#define POS_MAX           (7)
#define POS_MIN_CHAR      ('a')
#define POS_MAX_CHAR      ('h')
#define POS_MIN_CHAR_UC   ('A')
#define POS_MAX_CHAR_UC   ('H')

enum {
  POS_OK,
  ERR_INV_POS,
  ERR_INV_COLOR
};

class ChessPosition
{
public:
  int8_t x;
  int8_t y;

  ChessPosition();
  ChessPosition(int8_t x, int8_t y);

  uint8_t set(int8_t x, int8_t y);

  void clear();
};

uint8_t isValidPosition(const ChessPosition* pos);
uint8_t isValidPosition(const ChessPosition& pos);
uint8_t isValidPosition(const int8_t& pos);
uint8_t isValidPosition(const int8_t& x, const int8_t& y);

uint8_t getMovesDist(const ChessPosition& pos, int8_t dist, uint8_t pattern, ChessPosition *moves_dest, uint16_t *size_dest);

// Status codes for {move} & {getMove} functions
enum E_MOVE_STATUS {
  MOVE_OK,
  MOVE_ERR_INV_PIECE, //(todo needed?)
  MOVE_ERR_INV_POS,
  MOVE_ERR_ILL_POS,
  MOVE_ERR_INV_PATTERN,
  MOVE_ERR_INV_DEST,
  MOVE_ERR_INV_DIST,
  MOVE_ERR_INV_COLOR,
  MOVE_ERR_DEST_NULLPTR
};

// Bitmask for chess moves
#define M_SQUERE        0x01  // King
#define M_DIAGONAL      0x02  // Bishop, Queen
#define M_HOR_VER       0x04  // Rook, Queen
#define M_PAWN_TAKE     0x08  // ***free***
#define M_FORWARD       0x10  // Pawn
#define M_L_JUMP        0x20  // Knight
#define M_EN_PASSANT    0x40  // ***free*** Pawn takes sideways after quick forward pawn move
#define M_CASTLE        0x80  // Special move (king and rook in 1 turn)

#endif // CHESS_POSITION_H
