﻿#ifndef CHESSGAMEENGINE_H
#define CHESSGAMEENGINE_H

#include "../../Pieces/inc/piece.h"
#include "../../Pieces/inc/king.h"
#include "../../Pieces/inc/queen.h"
#include "../../Pieces/inc/rook.h"
#include "../../Pieces/inc/bishop.h"
#include "../../Pieces/inc/knight.h"
#include "../../Pieces/inc/pawn.h"

#include "../inc/chess_position.h"

#include <vector>

#define ENGINE_OK   0
#define ENGINE_ERR  1
//Todo more error types

class ChessGameEngine
{
private:
  std::vector<Piece *> pieces;

  std::vector<Piece *> pieces_black;
  std::vector<Piece *> pieces_white;

  // game stuff
  ChessPosition en_passant;
  ChessPosition king_white;
  ChessPosition king_black;
  ChessPosition attacker;

  uint8_t _setupPlayer(uint8_t player);
  void _setupboard(uint8_t variation);

  typedef struct
  {
    int8_t x;
    int8_t y;
  } deltaCoordinate;

  void _updateLegalMoves();

  void _addLegalMoves(deltaCoordinate *delta, uint8_t size);

  uint8_t _checkedAfterMove(uint8_t move);

  void _verifyLegalMoves();

  uint8_t _calcLegalMoves();

  uint8_t _checkAtackers();

  uint8_t _checkedMoves();

  void _updateThreatMask(uint8_t player);

  void _addThreaths(uint8_t player, ChessPosition pos, deltaCoordinate *delta, uint8_t size);

  uint8_t _getPieceAt(int8_t x, int8_t y,  Piece **dest);

  uint8_t _getPiece(Piece **dest);

protected:
  int8_t selectedIndex;

  int8_t player_turn;

  ChessPosition *moves;
  uint16_t moves_count;

  ChessPosition *legal_moves;
  uint16_t legal_moves_count;

  uint64_t threat_mask_white;
  uint64_t threat_mask_black;

  uint8_t capture(int8_t x, int8_t y);
  //uint8_t capture(ChessPosition pos, ChessPosition capture);
  //uint8_t capture(ChessPosition capture);

  void setNextToPlay(int8_t player_turn);

public:
  ChessGameEngine();
  ~ChessGameEngine();

  uint8_t startGame();

  uint8_t loadExcample(uint8_t example_id);

  uint8_t calcCheckedMoves();

  uint8_t move(int8_t x, int8_t y, int8_t dx, int8_t dy);
  //uint8_t move(ChessPosition pos, ChessPosition dest);
  uint8_t move(ChessPosition);

  int8_t getSelectedIndex();

  uint8_t select(int8_t x, int8_t y);
  uint8_t select(ChessPosition pos);
  void setSelectedIndex(int8_t newSelectedIndex);

  uint8_t discard_select();

  uint8_t nextToPlay();

  ChessPosition selectedPosition();

  uint8_t isCheck();

  uint8_t isCheckMate(); // rn also checked handler

  uint8_t isDraw(); //todo

  uint8_t getLegalMoves(); // rn updates legal moves

  uint8_t getBoard(std::vector<Piece *> *dest);
  uint8_t getPieces(std::vector<Piece *> *dest, uint8_t player = CPL_NONE);

  ChessPosition getKingPosition();

  uint8_t getMoves(int8_t x, int8_t y);
  uint8_t getMoves(int8_t x, int8_t y, uint8_t player);
  uint8_t getMoves(ChessPosition pos);
  uint8_t getMoves(ChessPosition pos, uint8_t player);
  uint8_t getMoves();

  uint8_t getType(int8_t x, int8_t y);
  uint8_t getType(ChessPosition pos);

  uint8_t getColor(int8_t x, int8_t y);
  uint8_t getColor(ChessPosition pos);

};

#endif // CHESSGAMEENGINE_H
