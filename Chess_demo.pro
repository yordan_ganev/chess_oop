TEMPLATE = app

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets



# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


CONFIG += console c++11
#CONFIG -= app_bundle
#CONFIG -= qt

SOURCES += \
        Chess/src/chess_position.cpp \
        Chess/src/chessgameengine.cpp \
        GUI/chessgui.cpp \
        Pieces/src/bishop.cpp \
        Pieces/src/king.cpp \
        Pieces/src/knight.cpp \
        Pieces/src/pawn.cpp \
        Pieces/src/piece.cpp \
        Pieces/src/queen.cpp \
        Pieces/src/rook.cpp \
        Tests/chessgameenginetest.cpp \
        Tests/chesspositiontest.cpp \
        Tests/piecesmanualtest.cpp \
        main.cpp

HEADERS += \
  Chess/inc/chess_position.h \
  Chess/inc/chessgameengine.h \
  GUI/chessgui.h \
  Pieces/inc/bishop.h \
  Pieces/inc/king.h \
  Pieces/inc/knight.h \
  Pieces/inc/pawn.h \
  Pieces/inc/piece.h \
  Pieces/inc/queen.h \
  Pieces/inc/rook.h \
  Tests/chessgameenginetest.h \
  Tests/chesspositiontest.h \
  Tests/piecesmanualtest.h

FORMS += \
  GUI/chessgui.ui


# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
  GUI/Images/chess_set.png

